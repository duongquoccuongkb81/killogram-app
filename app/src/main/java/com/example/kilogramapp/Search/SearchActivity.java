package com.example.kilogramapp.Search;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.ApolloCallback;
import com.apollographql.apollo.api.OperationDataJsonSerializer;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;
import com.apollographql.apollo.fetcher.ApolloResponseFetchers;
import com.example.kilogramapp.Application.GlobalApplication;
import com.example.kilogramapp.Login.LoginActivity;
import com.example.kilogramapp.PostQuery;
import com.example.kilogramapp.R;
import com.example.kilogramapp.RecycleViewComponent.DataClass;
import com.example.kilogramapp.RecycleViewComponent.DataCmt;
import com.example.kilogramapp.RecycleViewComponent.DataSearch;
import com.example.kilogramapp.RecycleViewComponent.NoiDungChat;
import com.example.kilogramapp.SearchUsersQuery;
import com.example.kilogramapp.Utils.BinhLuanAdapter;
import com.example.kilogramapp.Utils.BottomNavigationViewHelper;
import com.example.kilogramapp.Utils.ChatAdapter;
import com.example.kilogramapp.Utils.SearchAdapter;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;
import com.orhanobut.logger.Logger;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.function.Consumer;

import jp.wasabeef.blurry.Blurry;

@SuppressLint("Registered")
public class SearchActivity extends AppCompatActivity {
    private static final String TAG = "SearchActivity";
    private static final int ACTIVITY_NUM = 1;
    ArrayList<DataSearch> dataSearches = new ArrayList<>();
    RecyclerView rwSearh;
    SearchAdapter searchAdapter;
    EditText editSearch;
    private Context mContext = SearchActivity.this;
    private Handler uiHandler = new Handler(Looper.getMainLooper());
    private GlobalApplication application;
    private ApolloCall<SearchUsersQuery.Data> requestSearchUsers;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        Log.d(TAG, "onCreate: started");
        rwSearh = (RecyclerView)findViewById(R.id.rwsearch);
        rwSearh.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        rwSearh.setLayoutManager(layoutManager);
        ImageView imgSearch = (ImageView)findViewById(R.id.sendsearch);
        editSearch = (EditText)findViewById(R.id.textsearch);
        setupBottomNavigationView();
        application = (GlobalApplication) getApplication();
        application.createSharedPreferences(mContext);
        String token = application.getToken();
        if(token == null) {
            Toast.makeText(mContext, "Authenticated Fail", Toast.LENGTH_SHORT).show();
            application.removeStoredValue();
            Intent intentLogin = new Intent(mContext, LoginActivity.class);
            intentLogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intentLogin);
        }
        application.createApolloClient(token);
        imgSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dataSearches.clear();
                initView();
                populateData();
            }
        });
    }
//    public void insertItem(int position) {
//            dataSearches.add(position, new DataSearch(1,editSearch.getText().toString() + position +"","2 phút trước"));
//            searchAdapter.notifyItemInserted(position);
//    }
    private ApolloCall.Callback<SearchUsersQuery.Data> requestSearchUsersCallBack
            = new ApolloCallback<>(new ApolloCall.Callback<SearchUsersQuery.Data>() {
        @Override
        public void onResponse(@NotNull Response<SearchUsersQuery.Data> response) {
            if(response.hasErrors()) {
                String errorMessage = response.errors().get(0).message();
                Logger.e(errorMessage);
                if (errorMessage.contains("Authenticated Fail") ||  errorMessage.contains("jwt")) {
                    application.removeStoredValue();
                    Intent intentLogin = new Intent(mContext, LoginActivity.class);
                    intentLogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intentLogin);
                }
            } else {
                String resultJSON = OperationDataJsonSerializer.serialize(response.data(), " ");
                Logger.d(resultJSON);

                response.data().users().forEach(new Consumer<SearchUsersQuery.User>() {
                    @Override
                    public void accept(SearchUsersQuery.User user) {
                        DataSearch newUserSearch = new DataSearch(user);
                        dataSearches.add(newUserSearch);
                    }
                });
                initView();
            }
        }
        @Override
        public void onFailure(@NotNull ApolloException e) {
            Logger.e(e.getLocalizedMessage());
            Logger.e(String.valueOf(e.getCause()));
        }
    }, uiHandler);
    private void populateData() {
        String keyword = editSearch.getText().toString();
        Logger.d(keyword);
        if(!isStringNull(keyword)) {
            requestSearchUsers = application.apolloClient()
                    .query(SearchUsersQuery
                            .builder()
                            .keyword(keyword)
                            .build()).responseFetcher(ApolloResponseFetchers.NETWORK_FIRST);
            requestSearchUsers.enqueue(requestSearchUsersCallBack);
        }

    }
    private boolean isStringNull(String string){
        Log.d(TAG, "isStringNull: checking string if null");

        if (string.equals("")){
            return true;
        }
        return false;
    }
    public void initView(){
        //DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this,layoutManager.getOrientation());
        //recyclerView.addItemDecoration(dividerItemDecoration);
        searchAdapter= new SearchAdapter(dataSearches,this.mContext);
        rwSearh.setAdapter(searchAdapter);
        //recyclerView1.scrollToPosition(recyclerView1.getAdapter().getItemCount()-1);
    }
    private void setupBottomNavigationView() {
        Log.d(TAG, "setupBottomNavigationView: setting up");
        BottomNavigationViewEx bnve = (BottomNavigationViewEx) findViewById(R.id.bottomNavViewBar);
        BottomNavigationViewHelper.setupBottomNavigationView(bnve);
        BottomNavigationViewHelper.enableNavigation(mContext, bnve);
        Menu menu = bnve.getMenu();
        MenuItem menuItem = menu.getItem(ACTIVITY_NUM);
        menuItem.setChecked(true);
    }


}
