package com.example.kilogramapp.RecycleViewComponent;

import com.example.kilogramapp.FavoritesByPostQuery;

public class DataLikeFollow {
    FavoritesByPostQuery.LikedBy user;

    public DataLikeFollow(FavoritesByPostQuery.LikedBy user) {
        this.user = user;
    }

    public FavoritesByPostQuery.LikedBy getUser() {
        return user;
    }

    public void setUser(FavoritesByPostQuery.LikedBy user) {
        this.user = user;
    }
}
