package com.example.kilogramapp.RecycleViewComponent;

import com.example.kilogramapp.SearchUsersQuery;

public class DataSearch {
    public DataSearch(SearchUsersQuery.User mUser) {
        this.mUser = mUser;
    }
    private SearchUsersQuery.User mUser;

    public SearchUsersQuery.User getmUser() {
        return mUser;
    }

    public void setmUser(SearchUsersQuery.User mUser) {
        this.mUser = mUser;
    }
}
