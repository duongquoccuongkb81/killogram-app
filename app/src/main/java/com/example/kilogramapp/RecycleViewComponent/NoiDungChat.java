package com.example.kilogramapp.RecycleViewComponent;

import com.example.kilogramapp.ConversationByIDQuery;

public class NoiDungChat {
    private ConversationByIDQuery.Message message;

    public NoiDungChat(ConversationByIDQuery.Message message) {
        this.message = message;
    }

    public ConversationByIDQuery.Message getMessage() {
        return message;
    }

    public void setMessage(ConversationByIDQuery.Message message) {
        this.message = message;
    }


}
