package com.example.kilogramapp.RecycleViewComponent;

import com.example.kilogramapp.MyFavoritesQuery;

public class DataNotify {
    private MyFavoritesQuery.MyFavorite favorite;

    public MyFavoritesQuery.MyFavorite getFavorite() {
        return favorite;
    }

    public void setFavorite(MyFavoritesQuery.MyFavorite favorite) {
        this.favorite = favorite;
    }

    public DataNotify(MyFavoritesQuery.MyFavorite favorite) {
        this.favorite = favorite;
    }
}
