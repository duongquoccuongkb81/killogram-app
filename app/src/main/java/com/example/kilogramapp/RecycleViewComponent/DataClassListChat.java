package com.example.kilogramapp.RecycleViewComponent;

import com.example.kilogramapp.MyConversationsQuery;

public class DataClassListChat {

    private MyConversationsQuery.MyConversation Conversation;

    public DataClassListChat(MyConversationsQuery.MyConversation conversation) {
        Conversation = conversation;
    }

    public MyConversationsQuery.MyConversation getConversation() {
        return Conversation;
    }

    public void setConversation(MyConversationsQuery.MyConversation conversation) {
        Conversation = conversation;
    }
}
