package com.example.kilogramapp.RecycleViewComponent;

import com.example.kilogramapp.PostQuery;

public class DataClass {
    public DataClass(int nutchucnang, int mau, int binhluan, String like, int react, PostQuery.FollowingPost mPost) {
        this.nutchucnang = nutchucnang;
        this.mau = mau;
        Binhluan = binhluan;
        Like = like;
        React = react;
        this.mPost = mPost;
    }
    public DataClass(){

    }

    public int getNutchucnang() {
        return nutchucnang;
    }

    public void setNutchucnang(int nutchucnang) {
        this.nutchucnang = nutchucnang;
    }

    public int getMau() {
        return mau;
    }

    public void setMau(int mau) {
        this.mau = mau;
    }

    public int getBinhluan() {
        return Binhluan;
    }

    public void setBinhluan(int binhluan) {
        Binhluan = binhluan;
    }

    public String getLike() {
        return Like;
    }

    public void setLike(String like) {
        Like = like;
    }

    public int getReact() {
        return React;
    }

    public void setReact(int react) {
        React = react;
    }

    public PostQuery.FollowingPost getmPost() {
        return mPost;
    }

    public void setmPost(PostQuery.FollowingPost mPost) {
        this.mPost = mPost;
    }

    public String getID_baoviet() {
        return ID_baoviet;
    }

    public void setID_baoviet(String ID_baoviet) {
        this.ID_baoviet = ID_baoviet;
    }

    private String ID_baoviet;
    private String ID_nguoi;
    private int nutchucnang;
    private int mau;
    private int Binhluan;
    private String Like;
    private int React;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    private String content;
    public PostQuery.FollowingPost mPost;
}
