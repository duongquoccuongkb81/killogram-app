package com.example.kilogramapp.RecycleViewComponent;

import com.example.kilogramapp.FindPostByIdQuery;

public class DataCmt {
    public DataCmt(FindPostByIdQuery.Comment mComment) {
        this.mComment = mComment;
    }

    public FindPostByIdQuery.Comment getmComment() {
        return mComment;
    }

    public void setmComment(FindPostByIdQuery.Comment mComment) {
        this.mComment = mComment;
    }

    private FindPostByIdQuery.Comment mComment;


}
