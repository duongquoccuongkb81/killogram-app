package com.example.kilogramapp.Utils;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.MenuItem;

import androidx.annotation.NonNull;

import com.example.kilogramapp.Like.LikeActivity;
import com.example.kilogramapp.Main.MainActivity;
import com.example.kilogramapp.Profile.ProfileActivity;
import com.example.kilogramapp.R;
import com.example.kilogramapp.Search.SearchActivity;
import com.example.kilogramapp.Share.ShareActivity;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;

public class BottomNavigationViewHelper {
    private static final String TAG = "BottomNavigationViewHel";
    public static void setupBottomNavigationView(BottomNavigationViewEx bnve) {
        Log.d(TAG, "setupBottomNavigationView: setting up");
        bnve.enableAnimation(false);
        bnve.enableItemShiftingMode(false);
        bnve.enableShiftingMode(false);
        bnve.setTextVisibility(false);
    }
    public static void enableNavigation(final Context context, BottomNavigationViewEx bnve) {
        //final Context context để đưa vào được trong Override method
        bnve.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.ic_house:
                        Intent intentOne = new Intent(context, MainActivity.class);
                        context.startActivity(intentOne);
                        break;
                    case R.id.ic_search:
                        Intent intentTwo = new Intent(context, SearchActivity.class);
                        context.startActivity(intentTwo);
                        break;
                    case R.id.ic_circle_plus:
                        Intent intentThree = new Intent(context, ShareActivity.class);
                        context.startActivity(intentThree);
                        break;
                    case R.id.ic_heart_alert:
                        Intent intentFour = new Intent(context, LikeActivity.class);
                        context.startActivity(intentFour);
                        break;
                    case R.id.ic_android:
                        Intent intenFive = new Intent(context, ProfileActivity.class);
                        context.startActivity(intenFive);
                        break;
                }
                return false;
            }
        });
    }
}
