package com.example.kilogramapp.Utils;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.kilogramapp.Main.ActivityChat;
import com.example.kilogramapp.Main.Comment.extend;
import com.example.kilogramapp.Main.MainActivity;
import com.example.kilogramapp.Main.MainFragment;
import com.example.kilogramapp.R;
import com.example.kilogramapp.RecycleViewComponent.DataClassListChat;
import com.example.kilogramapp.RecycleViewComponent.DataCmt;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class ListFriendAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    //Danh sách các người bạn đã chat
    private static final String TAG = "ListFriendAdapter";
    private static final String CHUYENID ="chuyenID";
    ArrayList<DataClassListChat> DS;
    Context context;


    public ListFriendAdapter(ArrayList<DataClassListChat> arrChat, Context context) {
        this.DS = arrChat;
        this.context = context;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        View view = layoutInflater.inflate(R.layout.item_listchat,viewGroup,false);
        return new viewListHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewholder, int position) {
        populateListRows((ListFriendAdapter.viewListHolder) viewholder, position);

    }

    @Override
    public int getItemCount() {
        return DS == null ? 0 : DS.size();
    }


    public class viewListHolder extends RecyclerView.ViewHolder{
        ImageView hinhAnh;
        TextView tenNguoi;
        TextView thoiGian;
        CardView cardView;
        String conversationId;
        public viewListHolder(@NonNull View itemView) {
            super(itemView);
            hinhAnh = (ImageView)itemView.findViewById(R.id.avt);
            tenNguoi = (TextView)itemView.findViewById(R.id.friend);
            thoiGian = (TextView)itemView.findViewById(R.id.thoigianchat);
            cardView =(CardView)itemView.findViewById(R.id.nguoichat);
            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Dialog dialog = new Dialog("Chào");
                    Intent a = new Intent(itemView.getContext(), ActivityChat.class);
                    a.putExtra(CHUYENID, conversationId);
                    itemView.getContext().startActivity(a);
                }
            });

        }

    }
    private void populateListRows(ListFriendAdapter.viewListHolder viewHolder, int i) {
        viewHolder.tenNguoi.setText(DS.get(i).getConversation().withUser().username());
        viewHolder.thoiGian.setText(DS.get(i).getConversation().updatedAt().toString());
        String url = context.getResources().getString(R.string.default_avatar);
        if (DS.get(i).getConversation().withUser().profile().avatar() != null) {
            url = DS.get(i).getConversation().withUser().profile().avatar().path();
        }
        Picasso.get().load(url).into(viewHolder.hinhAnh);
        viewHolder.conversationId = DS.get(i).getConversation().id();
    }
}
