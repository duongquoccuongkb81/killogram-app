package com.example.kilogramapp.Utils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.kilogramapp.Application.GlobalApplication;
import com.example.kilogramapp.R;
import com.example.kilogramapp.RecycleViewComponent.DataCmt;
import com.example.kilogramapp.RecycleViewComponent.NoiDungChat;
import com.orhanobut.logger.Logger;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class ChatAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    ArrayList<NoiDungChat> nd;
    Context context;
    GlobalApplication application;
    String userId;
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
            View view = layoutInflater.inflate(R.layout.item_you,viewGroup,false);
            return new ChatAdapter.viewChatHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewholder, int position) {
        populateChatRows((ChatAdapter.viewChatHolder) viewholder, position);
    }

    @Override
    public int getItemCount() {
        return nd == null ? 0 : nd.size();
    }

    public ChatAdapter(ArrayList<NoiDungChat> nd, Context context) {
        this.nd = nd;
        this.context = context;
        application = (GlobalApplication) context.getApplicationContext();
        application.createSharedPreferences(context);
        userId = application.getUserId();

    }

    public class viewChatHolder extends RecyclerView.ViewHolder{
        TextView myMessage;
        TextView withMessage;
        public viewChatHolder(@NonNull View itemView) {
            super(itemView);
            //myMessage = (TextView)itemView.findViewById(R.id.ndchat);
            myMessage = (TextView) itemView.findViewById(R.id.myMessage);
            withMessage = (TextView)itemView.findViewById(R.id.nguoigui);

        }

    }

    private void populateChatRows(ChatAdapter.viewChatHolder viewHolder, int i) {

            if (nd.get(i).getMessage().sender().id().equals(userId)) {
                viewHolder.myMessage.setText(nd.get(i).getMessage().text());
                viewHolder.myMessage.setVisibility(View.VISIBLE);
            } else {
                viewHolder.withMessage.setText(nd.get(i).getMessage().text());
                viewHolder.withMessage.setVisibility(View.VISIBLE);
            //viewHolder.myMessage.setVisibility(View.GONE);
            }
    }

}
