package com.example.kilogramapp.Utils;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.kilogramapp.Application.GlobalApplication;
import com.example.kilogramapp.PostQuery;
import com.example.kilogramapp.Profile.ProfileActivity;
import com.example.kilogramapp.Profile.ViewProfileActivity;
import com.example.kilogramapp.R;
import com.example.kilogramapp.RecycleViewComponent.DataClass;
import com.example.kilogramapp.RecycleViewComponent.DataSearch;
import com.example.kilogramapp.RecycleViewComponent.NoiDungChat;
import com.orhanobut.logger.Logger;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class SearchAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    ArrayList<DataSearch> datasearch;
    Context context;
    private GlobalApplication application;
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        View view = layoutInflater.inflate(R.layout.item_search,viewGroup,false);

        return new SearchAdapter.viewSearchHolder(view);
    }


    public SearchAdapter(ArrayList<DataSearch> nd, Context context) {
        this.datasearch = nd;
        this.context = context;
        application = (GlobalApplication) context.getApplicationContext();
        application.createSharedPreferences(context);
    }
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewholder, int position) {
        populateSearchRows((SearchAdapter.viewSearchHolder) viewholder, position);
    }

    @Override
    public int getItemCount() {
        return datasearch == null ? 0 : datasearch.size();
    }

    public class viewSearchHolder extends RecyclerView.ViewHolder{
        String searchID;
        TextView nameUser;
        ImageView imageUser;
        TextView nameName;
        CardView linearS;
        public viewSearchHolder(@NonNull View itemView) {
            super(itemView);
            nameUser = (TextView)itemView.findViewById(R.id.searchname);
            imageUser = (ImageView)itemView.findViewById(R.id.searchavt);
            nameName = (TextView)itemView.findViewById(R.id.nameview);
            linearS = (CardView) itemView.findViewById(R.id.cardsearch);
            linearS.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String userId = application.getUserId();
                    if (userId.equals(searchID)) {
                        Intent intent  = new Intent(context, ProfileActivity.class);
                        context.startActivity(intent);
                    } else {
                        Intent intent  = new Intent(context, ViewProfileActivity.class);
                        intent.putExtra("userId", searchID);
                        context.startActivity(intent);
                    }
                }
            });


        }

    }

    private void populateSearchRows(SearchAdapter.viewSearchHolder viewHolder, int i) {
        viewHolder.nameUser.setText(datasearch.get(i).getmUser().username());
        viewHolder.nameName.setText(datasearch.get(i).getmUser().profile().name());
        viewHolder.searchID = datasearch.get(i).getmUser().id();
        String url = "https://www.kindpng.com/picc/m/404-4042814_facebook-no-profile-png-download-default-headshot-transparent.png";
        if (datasearch.get(i).getmUser().profile().avatar() == null) {
            Picasso.get().load(url).into(viewHolder.imageUser);
        } else {
            Picasso.get().load(datasearch.get(i).getmUser().profile().avatar().path()).into(viewHolder.imageUser);
        }


    }

}
