package com.example.kilogramapp.Utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.api.OperationDataJsonSerializer;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;
import com.example.kilogramapp.Application.GlobalApplication;
import com.example.kilogramapp.FollowMutation;
import com.example.kilogramapp.Login.LoginActivity;
import com.example.kilogramapp.Profile.ProfileActivity;
import com.example.kilogramapp.Profile.ViewProfileActivity;
import com.example.kilogramapp.R;
import com.example.kilogramapp.RecycleViewComponent.DataLikeFollow;
import com.example.kilogramapp.RecycleViewComponent.NoiDungChat;
import com.example.kilogramapp.UnFollowMutation;
import com.orhanobut.logger.Logger;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;

public class LFAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    ArrayList<DataLikeFollow> data = new ArrayList<DataLikeFollow>();
    Context context;
    ApolloCall<FollowMutation.Data> requestFollowUser;
    ApolloCall<UnFollowMutation.Data> requestUnFollowUser;
    private GlobalApplication application;
    private String ownerId;
    public LFAdapter(ArrayList<DataLikeFollow> nd, Context context) {
        this.data = nd;
        this.context = context;
        application = (GlobalApplication) context.getApplicationContext();
        application.createSharedPreferences(context);
        String token = application.getToken();
        if(token == null) {
            Toast.makeText(context, "Authenticated Fail", Toast.LENGTH_SHORT).show();
            Intent intentLogin = new Intent(context, LoginActivity.class);
            intentLogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            context.startActivity(intentLogin);
        }
        application.createApolloClient(token);
        ownerId = application.getUserId();
    }
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        View view = layoutInflater.inflate(R.layout.item_likefollow,viewGroup,false);
        return new LFAdapter.viewLFHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewholder, int position) {
        populateLFRows((LFAdapter.viewLFHolder) viewholder, position);
    }


    public class viewLFHolder extends RecyclerView.ViewHolder{
        CircleImageView avtfollow;
        TextView username;
        TextView namename;
        String userId;
        TextView statusFollowed;
        String followed;
        CardView nguoifollow;
        public viewLFHolder(@NonNull View itemView) {
            super(itemView);
            avtfollow = (CircleImageView)itemView.findViewById(R.id.avtlf);
            username = (TextView)itemView.findViewById(R.id.userlf);
            namename = (TextView)itemView.findViewById(R.id.namelf);
            statusFollowed = itemView.findViewById(R.id.textBtnFollow);
            nguoifollow = (CardView)itemView.findViewById(R.id.nguoifollow);
            nguoifollow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String id = application.getUserId();
                    if (id.equals(userId)) {
                        Intent intent  = new Intent(itemView.getContext(), ProfileActivity.class);
                        itemView.getContext().startActivity(intent);
                    } else {
                        Intent intent  = new Intent(itemView.getContext(), ViewProfileActivity.class);
                        intent.putExtra("userId", userId);
                        itemView.getContext().startActivity(intent);
                    }
                }
            });
            statusFollowed.setOnClickListener(v -> {
                statusFollowed.setEnabled(false);
                Context contextView = itemView.getContext();
                Activity activity = (Activity) contextView;
                if (!userId.equals(ownerId)) {
                    if (followed == "False") {
                        requestFollowUser = application.apolloClient()
                                .mutate(FollowMutation
                                        .builder()
                                        .userId(userId)
                                        .build());
                        requestFollowUser.enqueue(new ApolloCall.Callback<FollowMutation.Data>() {
                            @Override
                            public void onResponse(@NotNull Response<FollowMutation.Data> response) {
                                if (response.hasErrors()) {
                                    String errorMessage = response.errors().get(0).message();
                                    Logger.e(errorMessage);
                                    if (errorMessage.contains("Authenticated Fail") ||  errorMessage.contains("jwt")) {
                                        application.removeStoredValue();
                                        Intent intentLogin = new Intent(context, LoginActivity.class);
                                        intentLogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        context.startActivity(intentLogin);
                                    } else if (errorMessage.contains("Follow")) {
                                        activity.runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                statusFollowed.setEnabled(true);
                                                Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show();
                                            }
                                        });
                                    }
                                } else {
                                    assert response.data() != null;
                                    String dataJSON = OperationDataJsonSerializer.serialize(response.data(), " ");
                                    Logger.d(dataJSON);
                                    activity.runOnUiThread(new Runnable() {
                                        @SuppressLint("ResourceAsColor")
                                        @Override
                                        public void run() {
                                            statusFollowed.setEnabled(true);
                                            statusFollowed.setText("Đang theo dõi");
                                            statusFollowed.setTextColor(R.color.black);
                                            statusFollowed.setBackgroundResource(R.drawable.grey_border_rounded);
                                            followed = "True";
                                        }
                                    });
                                }
                            }

                            @Override
                            public void onFailure(@NotNull ApolloException e) {
                                activity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        statusFollowed.setEnabled(true);
                                    }
                                });
                                Logger.e(Objects.requireNonNull(e.getLocalizedMessage()));
                            }
                        });
                    } else if (followed == "True") {
                        requestUnFollowUser = application.apolloClient()
                                .mutate(UnFollowMutation
                                        .builder()
                                        .userId(userId)
                                        .build());
                        requestUnFollowUser.enqueue(new ApolloCall.Callback<UnFollowMutation.Data>() {
                            @Override
                            public void onResponse(@NotNull Response<UnFollowMutation.Data> response) {
                                if (response.hasErrors()) {
                                    String errorMessage = response.errors().get(0).message();
                                    Logger.e(errorMessage);
                                    if (errorMessage.contains("Authenticated Fail") ||  errorMessage.contains("jwt")) {
                                        application.removeStoredValue();
                                        Intent intentLogin = new Intent(context, LoginActivity.class);
                                        intentLogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        context.startActivity(intentLogin);
                                    } else if (errorMessage.contains("Follow")) {
                                        activity.runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                statusFollowed.setEnabled(true);
                                                Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show();
                                            }
                                        });
                                    }
                                } else {
                                    assert response.data() != null;
                                    String dataJSON = OperationDataJsonSerializer.serialize(response.data(), " ");
                                    Logger.d(dataJSON);
                                    activity.runOnUiThread(new Runnable() {
                                        @SuppressLint("ResourceAsColor")
                                        @Override
                                        public void run() {
                                            statusFollowed.setEnabled(true);
                                            statusFollowed.setText("Theo dõi");
                                            statusFollowed.setTextColor(R.color.white);
                                            statusFollowed.setBackgroundResource(R.drawable.buttonfollow);
                                            followed = "False";
                                        }
                                    });
                                }
                            }

                            @Override
                            public void onFailure(@NotNull ApolloException e) {
                                activity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        statusFollowed.setEnabled(true);
                                    }
                                });
                                Logger.e(Objects.requireNonNull(e.getLocalizedMessage()));
                            }
                        });
                    }
                }
            });
        }
    }
    @Override
    public int getItemCount() {
        {
            return data == null ? 0 : data.size();
        }
    }
    @SuppressLint("ResourceAsColor")
    private void populateLFRows(LFAdapter.viewLFHolder viewHolder, int i) {
        viewHolder.userId = data.get(i).getUser().id();
        viewHolder.username.setText(data.get(i).getUser().username());
        viewHolder.namename.setText(data.get(i).getUser().profile().name());
        String url = context.getResources().getString(R.string.default_avatar);
        if (data.get(i).getUser().profile().avatar() != null) {
            url = data.get(i).getUser().profile().avatar().path();
        }
        Picasso.get().load(url).into(viewHolder.avtfollow);
        viewHolder.followed = data.get(i).getUser().followed().rawValue();
        if (viewHolder.followed == "True") {
            viewHolder.statusFollowed.setText("Đang theo dõi");
            viewHolder.statusFollowed.setTextColor(R.color.black);
            viewHolder.statusFollowed.setBackgroundResource(R.drawable.grey_border_rounded);
        } else if (viewHolder.followed == "False") {
            viewHolder.statusFollowed.setText("Theo dõi");
        } else {
            viewHolder.statusFollowed.setVisibility(View.GONE);
        }


    }
}
