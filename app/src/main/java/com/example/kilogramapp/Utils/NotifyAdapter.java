package com.example.kilogramapp.Utils;

import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.kilogramapp.Application.GlobalApplication;
import com.example.kilogramapp.FindPostByIdQuery;
import com.example.kilogramapp.Main.Comment.extend;
import com.example.kilogramapp.R;
import com.example.kilogramapp.RecycleViewComponent.DataLikeFollow;
import com.example.kilogramapp.RecycleViewComponent.DataNotify;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class NotifyAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    ArrayList<DataNotify> dataNotifies = new ArrayList<>();
    Context context;
    GlobalApplication application;
    String userId;
    public static final String CHUYENID ="chuyenID";
    public NotifyAdapter(ArrayList<DataNotify> nd, Context context) {
        this.dataNotifies = nd;
        this.context = context;
        application = (GlobalApplication) context.getApplicationContext();
        application.createSharedPreferences(context);
        userId = application.getUserId();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        View view = layoutInflater.inflate(R.layout.item_notify,viewGroup,false);
        return new NotifyAdapter.viewNoHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewholder, int position) {
        populateNoRows((NotifyAdapter.viewNoHolder) viewholder, position);
    }
    public class viewNoHolder extends RecyclerView.ViewHolder{
        CircleImageView avt;
        TextView userName;
        ImageView hinhanhlikes;
        TextView time;
        TextView content;
        String postId;
        RelativeLayout relativeLayout;
        public viewNoHolder(@NonNull View itemView) {
            super(itemView);
            avt = (CircleImageView)itemView.findViewById(R.id.avtnotify);
            userName = (TextView)itemView.findViewById(R.id.UserName);
            hinhanhlikes = (ImageView) itemView.findViewById(R.id.imgnotify);
            time = (TextView)itemView.findViewById(R.id.timenotify);
            content = itemView.findViewById(R.id.favorite_content);
            relativeLayout = (RelativeLayout)itemView.findViewById(R.id.layoutcmt);
            relativeLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent a = new Intent(itemView.getContext(), extend.class);

                    a.putExtra(CHUYENID,postId);
                    itemView.getContext().startActivity(a);
                }
            });
        }
    }
    private void populateNoRows(NotifyAdapter.viewNoHolder viewHolder, int i) {
        viewHolder.postId = dataNotifies.get(i).getFavorite().post().id();

        String url = context.getResources().getString(R.string.default_avatar);
        if(dataNotifies.get(i).getFavorite().likedBy().profile().avatar() != null) {
            url = dataNotifies.get(i).getFavorite().likedBy().profile().avatar().path();
        }
        Picasso.get().load(url).into(viewHolder.avt);
        String imagePath = dataNotifies.get(i).getFavorite().post().image().path();
        Picasso.get().load(imagePath).into(viewHolder.hinhanhlikes);
        viewHolder.time.setText(dataNotifies.get(i).getFavorite().createdAt().toString());
        if(dataNotifies.get(i).getFavorite().likedBy().id().equals(userId)){
            viewHolder.userName.setVisibility(View.GONE);
            viewHolder.content.setText("Bạn đã thích bài viết này");
        } else {
            viewHolder.userName.setText(dataNotifies.get(i).getFavorite().likedBy().username());
        }
    }
    @Override
    public int getItemCount() {
        {
            return dataNotifies == null ? 0 : dataNotifies.size();
        }
    }
}
