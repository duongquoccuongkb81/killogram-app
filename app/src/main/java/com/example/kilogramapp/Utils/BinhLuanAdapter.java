package com.example.kilogramapp.Utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.api.OperationDataJsonSerializer;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;
import com.example.kilogramapp.Application.GlobalApplication;
import com.example.kilogramapp.CreateCommentMutation;
import com.example.kilogramapp.DeleteCommentMutation;
import com.example.kilogramapp.FindPostByIdQuery;
import com.example.kilogramapp.Login.LoginActivity;
import com.example.kilogramapp.Profile.ProfileActivity;
import com.example.kilogramapp.Profile.ViewProfileActivity;
import com.example.kilogramapp.R;
import com.example.kilogramapp.RecycleViewComponent.DataClass;
import com.example.kilogramapp.RecycleViewComponent.DataCmt;
import com.google.gson.Gson;
import com.orhanobut.logger.Logger;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;
import org.w3c.dom.Text;

import java.util.ArrayList;

public class BinhLuanAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    ArrayList<DataCmt> dataCmts;
    Context context;
    private GlobalApplication application;
    private String userId;
    private Handler uiHandler = new Handler(Looper.getMainLooper());
    private ApolloCall<DeleteCommentMutation.Data> requestDeleteComment;
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup , int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        View view = layoutInflater.inflate(R.layout.item_cmt,viewGroup,false);
        return new viewCmtHolder(view);

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewholder, int position) {
        populateCmtRows((viewCmtHolder) viewholder, position);

    }

    @Override
    public int getItemCount() {
        return dataCmts == null ? 0 : dataCmts.size();
    }
    /*BinhLuanAdapter*/
    public BinhLuanAdapter(ArrayList<DataCmt> dataCmts, Context context) {
        this.dataCmts = dataCmts;
        this.context = context;
        application = (GlobalApplication) context.getApplicationContext();
        application.createSharedPreferences(context);
        userId = application.getUserId();
        String token = application.getToken();
        if(token == null) {
            Toast.makeText(context, "Authenticated Fail", Toast.LENGTH_SHORT).show();
            Intent intentLogin = new Intent(context, LoginActivity.class);
            intentLogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            context.startActivity(intentLogin);
        }
        application.createApolloClient(token);
    }

    public class viewCmtHolder extends RecyclerView.ViewHolder{
        ImageView hinhBL;
        TextView tenBL;
        TextView contentBL;
        TextView timeBL;

        ImageView xoaCmt;
        String idCmt;
        String idnguoicmt;

        public viewCmtHolder(@NonNull View itemView) {
            super(itemView);
            hinhBL = (ImageView)itemView.findViewById(R.id.HinhBinhLuan);
            tenBL =(TextView)itemView.findViewById(R.id.TenBinhLuan);
            contentBL=(TextView)itemView.findViewById(R.id.ContentBinhLuan);
            timeBL =(TextView)itemView.findViewById(R.id.Times);
            xoaCmt = (ImageView)itemView.findViewById(R.id.xoacmt);
            xoaCmt.setVisibility(View.GONE);
            tenBL.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String userId = application.getUserId();
                    if (userId.equals(idnguoicmt)) {
                        Intent intent  = new Intent(itemView.getContext(), ProfileActivity.class);
                        itemView.getContext().startActivity(intent);
                    } else {
                        Intent intent  = new Intent(itemView.getContext(), ViewProfileActivity.class);
                        intent.putExtra("userId", idnguoicmt);
                        itemView.getContext().startActivity(intent);
                    }
                }
            });
            /*Xóa cmt*/
            xoaCmt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Context contextView = itemView.getContext();
                    Activity activity = (Activity) contextView;
                    xoaCmt.setEnabled(false);
                    requestDeleteComment = application.apolloClient()
                            .mutate(DeleteCommentMutation
                                    .builder()
                                    .commentId(idCmt)
                                    .build());
                    requestDeleteComment.enqueue(new ApolloCall.Callback<DeleteCommentMutation.Data>() {
                        @Override
                        public void onResponse(@NotNull Response<DeleteCommentMutation.Data> response) {

                            if(response.hasErrors()) {
                                String errorMessage = response.errors().get(0).message();
                                Logger.e(errorMessage);
                                if (errorMessage.contains("Authenticated Fail") ||  errorMessage.contains("jwt")) {
                                    application.removeStoredValue();
                                    Intent intentLogin = new Intent(context, LoginActivity.class);
                                    intentLogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    context.startActivity(intentLogin);
                                }  else {
                                    activity.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            xoaCmt.setEnabled(true);
                                            Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show();
                                        }
                                    });

                                }
                            } else {
                                assert response.data() != null;
                                activity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        xoaCmt.setEnabled(true);
                                    }
                                });
                                String dataJSON = OperationDataJsonSerializer.serialize(response.data(), " ");
                                Logger.d(dataJSON);
                            }
                        }

                        @Override
                        public void onFailure(@NotNull ApolloException e) {
                            Logger.e(e.getLocalizedMessage());
                            Logger.e(String.valueOf(e.getCause()));
                            activity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    xoaCmt.setEnabled(true);
                                    Toast.makeText(context, e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                    });
                }
            });
        }



    }
    private void populateCmtRows(BinhLuanAdapter.viewCmtHolder viewHolder, int i) {
        viewHolder.tenBL.setText(dataCmts.get(i).getmComment().author().username());
        viewHolder.timeBL.setText(dataCmts.get(i).getmComment().createdAt().toString());
        viewHolder.contentBL.setText(dataCmts.get(i).getmComment().text());
        viewHolder.idCmt = dataCmts.get(i).getmComment().id();
        viewHolder.idnguoicmt = dataCmts.get(i).getmComment().author().id();
        String url = "https://www.kindpng.com/picc/m/404-4042814_facebook-no-profile-png-download-default-headshot-transparent.png";
        if(dataCmts.get(i).getmComment().author().profile().avatar() != null) {
            url = dataCmts.get(i).getmComment().author().profile().avatar().path();
        }
        Picasso.get().load(url).into(viewHolder.hinhBL);
        if(dataCmts.get(i).getmComment().author().id().equals(userId)) {
           viewHolder.xoaCmt.setVisibility(View.VISIBLE);

        }


    }



}
