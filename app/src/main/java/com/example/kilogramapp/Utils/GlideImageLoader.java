package com.example.kilogramapp.Utils;

import android.content.Context;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.example.kilogramapp.R;


public class GlideImageLoader {
    private static final String TAG = "GlideImageLoader";
    private static final int defaultImage = R.drawable.ic_loadmore;
    public static void setImage(Context context, String imageURL, ImageView image, String append) {
        String fullImageURL = append + imageURL;
        Glide.with(context)
                .load(fullImageURL)
                .placeholder(defaultImage)
                .fitCenter()
                .apply(RequestOptions.overrideOf(Target.SIZE_ORIGINAL))
                .into(image);
    }
}
