package com.example.kilogramapp.Utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.ApolloCallback;
import com.apollographql.apollo.api.OperationDataJsonSerializer;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.example.kilogramapp.Application.GlobalApplication;
import com.example.kilogramapp.FavoritePostMutation;
import com.example.kilogramapp.Login.LoginActivity;
import com.example.kilogramapp.Main.ActivityFL;
import com.example.kilogramapp.Main.ActivityLF;
import com.example.kilogramapp.Main.Comment.extend;
import com.example.kilogramapp.PostQuery;
import com.example.kilogramapp.Profile.ProfileActivity;
import com.example.kilogramapp.Profile.ViewProfileActivity;
import com.example.kilogramapp.R;
import com.example.kilogramapp.RecycleViewComponent.DataClass;
import com.example.kilogramapp.RemovePostMutation;
import com.example.kilogramapp.UnFavoritePostMutation;
import com.example.kilogramapp.UpdatePrivacyPostMutation;
import com.example.kilogramapp.UpdateTitlePostMutation;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.Gson;
import com.orhanobut.logger.Logger;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;


/*PostAdapter Class*/
public class PostAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final String CHUYENDATA = "chuyendata";
    public static final String CHUYENHINH = "chuey";
    public static final String CHUYENIMG = "chuyenhinhanh";
    public static final String CHUYENTITLE ="chuyentitle";
    public static final String CHUYENID ="chuyenID";
    public static final String CHUYENTK ="chuyen";

    ArrayList<DataClass> dataClass = new ArrayList<DataClass>();

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    private GlobalApplication application;
    private Handler uiHandler = new Handler(Looper.getMainLooper());
    private ApolloCall<RemovePostMutation.Data> requestRemovePost;
    private ApolloCall<UpdateTitlePostMutation.Data> requestUpdateTitlePost;
    private ApolloCall<FavoritePostMutation.Data> requestFavoritePost;
    private ApolloCall<UnFavoritePostMutation.Data> requestUnFavoritePost;
    private ApolloCall<UpdatePrivacyPostMutation.Data> requestUpdatePrivacyPost;
    private int currentPosition;

    Context context;
    public PostAdapter(){

    }
    /*PostAdapter*/
    public PostAdapter(ArrayList<DataClass> dataclass, Context context) {
        this.dataClass = dataclass;
        this.context = context;
        application = (GlobalApplication) context.getApplicationContext();
        application.createSharedPreferences(context);
        String token = application.getToken();
        application.createApolloClient(token);
    }

    public int GetPosition(String postID)
    {
        for(int i = 0;i<dataClass.size();i++)
        {
            if(dataClass.get(i).mPost.id().equals(postID))
            {
                return i;
            }
        }
        return -1;
    }
    /*LoadingView*/
    private void showLoadingView(Loading viewHolder, int position) {
        //ProgressBar would be displayed
    }

    private void populateItemRows(ViewHolder viewHolder, int i) {
        viewHolder.textView.setText(dataClass.get(i).mPost.author().username().toString());
        String url = dataClass.get(i).mPost.image().path();
        //Picasso.get().load(url).into(viewHolder.imgPost);
        //GlideImageLoader.setImage(context, url, viewHolder.imgPost, "");
        Glide.with(context)
                .load(url)
                .fitCenter()
                .centerCrop()
                //.override()
                .apply(RequestOptions.overrideOf(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL))
                .placeholder(R.drawable.ic_loadmore)
                .into(viewHolder.imgPost);

        String url2;
        if (dataClass.get(i).mPost.author().profile().avatar() == null) {
            url2 = "https://www.kindpng.com/picc/m/404-4042814_facebook-no-profile-png-download-default-headshot-transparent.png";
        } else {
            url2 = dataClass.get(i).mPost.author().profile().avatar().path();
        }
        Picasso.get().load(url2).into(viewHolder.imgView);
        viewHolder.idviet=dataClass.get(i).mPost.id();
        //GlideImageLoader.setImage(context, url2, viewHolder.imgView, "");
        //viewHolder.linearLayout.setId(Integer.parseInt(dataClass.get(i).mPost.id()));
        if(dataClass.get(i).mPost.favorited()==true)
        {
            dataClass.get(i).setReact(R.drawable.heart_x);
            viewHolder.imgReact.setImageResource(R.drawable.heart_x);
            dataClass.get(i).setMau(R.color.Heart);
        }
        else{
            viewHolder.imgReact.setImageResource(dataClass.get(i).getReact());
        }
        viewHolder.idtk =dataClass.get(i).mPost.author().id();
        viewHolder.time.setText(dataClass.get(i).mPost.createdAt().toString());
        dataClass.get(i).setContent(dataClass.get(i).mPost.title());
        viewHolder.conTent.setText(dataClass.get(i).getContent());
        //viewHolder.imgComment.setId(dataclass.get(i).mPost.id());
        viewHolder.idviet = dataClass.get(i).mPost.id();
        viewHolder.luotthich.setText(dataClass.get(i).mPost.favoritesCount()+"");
        viewHolder.luotbinhluan.setText(dataClass.get(i).mPost.commentsCount()+"");
        String userId = application.getUserId();
        //Log.d(userId, "populateItemRows: " + userId);
        if(userId.equals(dataClass.get(i).mPost.author().id()))
        {
            dataClass.get(i).setNutchucnang(R.drawable.more);
            viewHolder.imgSet.setImageResource(dataClass.get(i).getNutchucnang());

        }
        else
        {
            dataClass.get(i).setNutchucnang(0);
            viewHolder.imgSet.setImageResource(dataClass.get(i).getNutchucnang());
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        if (i == VIEW_TYPE_ITEM) {
            LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
            View view = layoutInflater.inflate(R.layout.item_row,viewGroup,false);
            return new ViewHolder(view);
        } else {
            LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
            View view = layoutInflater.inflate(R.layout.item_loading,viewGroup,false);
            return new Loading(view);
        }
    }



    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        if (viewHolder instanceof ViewHolder) {
            populateItemRows((ViewHolder) viewHolder, i);

        } else if (viewHolder instanceof Loading) {
            showLoadingView((Loading) viewHolder, i);
        }


    }

    private ApolloCall.Callback<RemovePostMutation.Data> requestRemovePostCallBack
            = new ApolloCallback<>(new ApolloCall.Callback<RemovePostMutation.Data>() {
        @Override
        public void onResponse(@NotNull Response<RemovePostMutation.Data> response) {
            if(response.hasErrors()) {
                String errorMessage = response.errors().get(0).message();
                Logger.e(errorMessage);
                if (errorMessage.contains("Authenticated Fail") ||  errorMessage.contains("jwt")) {
                    Intent intentLogin = new Intent(context, LoginActivity.class);
                    intentLogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    context.startActivity(intentLogin);

                } else {
                    Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show();
                }

            } else {
                String resultJSON = OperationDataJsonSerializer.serialize(response.data(), " ");
                Logger.d(resultJSON);
                dataClass.remove(currentPosition);

                notifyItemRemoved(currentPosition);
            }
        }

        @Override
        public void onFailure(@NotNull ApolloException e) {
            Logger.e(e.getLocalizedMessage());
            Logger.e(e.getCause().toString());
            Toast.makeText(context, e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
        }
    }, uiHandler);


    @Override
    public int getItemCount() {
        return dataClass == null ? 0 : dataClass.size();
    }

    public void RemoveItemUI(int position)
    {
        if(position !=-1) {
            dataClass.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void RemoveItem(int position)
    {
        if(position !=-1)
        {
            String postId = dataClass.get(position).mPost.id();
            currentPosition = position;
            requestRemovePost = application.apolloClient()
                    .mutate(RemovePostMutation
                            .builder().postId(postId).build());
            requestRemovePost.enqueue(requestRemovePostCallBack);
        }

    }

    public void addItem(DataClass db)
    {
        dataClass.add(0,db);
        notifyDataSetChanged();
    }

    public void updateSingleItem(int position, DataClass data) {
        dataClass.set(position, data);
        notifyItemChanged(position);
    }

    public int getItemViewType(int position) {
        return dataClass.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }
    public  class Loading extends RecyclerView.ViewHolder{
        ProgressBar pc;

        public Loading(@NonNull View itemView) {
            super(itemView);
            pc = (ProgressBar)itemView.findViewById(R.id.progressBar);
        }
    }
    public class ViewHolder extends RecyclerView.ViewHolder{
        boolean state =true;
        TextView textView;
        ImageView imgView;
        LinearLayout linearLayout;
        ImageView imgReact;
        TextView textLikes;
        ImageView imgComment;
        ImageView imgSet;
        TextView conTent;
        TextView luotthich;
        ImageView imgPost;
        String idviet;
        TextView time;
        TextView luotbinhluan;
        String idtk;

        public ViewHolder(final View itemView) {
            super(itemView);
            linearLayout = (LinearLayout)itemView.findViewById(R.id.linearLayout);
            textView = (TextView)itemView.findViewById(R.id.TextView);
            imgView = (ImageView)itemView.findViewById(R.id.TamHinh);
            imgReact = (ImageView)itemView.findViewById(R.id.react);
            imgComment = (ImageView)itemView.findViewById(R.id.binhluan);
            imgSet = (ImageView)itemView.findViewById(R.id.chucnang);
            conTent = (TextView)itemView.findViewById(R.id.Content);
            imgPost = (ImageView)itemView.findViewById(R.id.TamHinhND);
            luotthich = (TextView)itemView.findViewById(R.id.countLikes);
            time =(TextView)itemView.findViewById(R.id.TG);
            luotbinhluan =(TextView)itemView.findViewById(R.id.countbinhluan);
            idviet ="";
            idtk="";
            luotthich.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent a = new Intent(itemView.getContext(), ActivityFL.class);

                    a.putExtra(CHUYENTK,idviet);
                    itemView.getContext().startActivity(a);
                }
            });
            textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String userId = application.getUserId();
                    if (userId.equals(idtk)) {
                        Intent intent  = new Intent(itemView.getContext(), ProfileActivity.class);
                        itemView.getContext().startActivity(intent);
                    } else {
                        Intent intent  = new Intent(itemView.getContext(), ViewProfileActivity.class);
                        intent.putExtra("userId", idtk);
                        itemView.getContext().startActivity(intent);
                    }
                }
            });
            imgComment.setOnClickListener(new View.OnClickListener() {
                @Override
               public void onClick(View v) {
                    //RemoveItem(getAdapterPosition());
                    int s = v.getId();
                    Intent a = new Intent(itemView.getContext(), extend.class);
                    int layvitri = getAdapterPosition();
                    a.putExtra(CHUYENID,idviet);
                    itemView.getContext().startActivity(a);
                }
            });
            imgReact.setOnClickListener(new View.OnClickListener() {

                @SuppressLint("ResourceAsColor")
                @Override
                public void onClick(View v) {
                    imgReact.setEnabled(false);
                    int position = getAdapterPosition();
                    int demchuongtrinh = dataClass.get(position).getReact();
                    String postId = dataClass.get(position).mPost.id();
                    Context contextView = itemView.getContext();
                    Activity activity = (Activity) contextView;
                    int favoritesCount = dataClass.get(position).mPost.favoritesCount();
                    switch (demchuongtrinh) {
                        case R.drawable.heart:{
                            imgReact.setImageResource(R.drawable.heart_x);
                            dataClass.get(position).setReact(R.drawable.heart_x);
                            luotthich.setText(++favoritesCount+"");
                            requestFavoritePost = application.apolloClient()
                                                    .mutate(FavoritePostMutation
                                                            .builder()
                                                            .postId(postId)
                                                            .build());
                            requestFavoritePost.enqueue(new ApolloCall.Callback<FavoritePostMutation.Data>() {
                                @Override
                                public void onResponse(@NotNull Response<FavoritePostMutation.Data> response) {
                                    if(response.hasErrors()) {
                                        activity.runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                imgReact.setEnabled(true);
                                                imgReact.setImageResource(R.drawable.heart);
                                                dataClass.get(position).setReact(R.drawable.heart);
                                            }
                                        });
                                        String errorMessage = response.errors().get(0).message();
                                        Logger.e(errorMessage);
                                        if (errorMessage.contains("Authenticated Fail") ||  errorMessage.contains("jwt")) {
                                            Intent intentLogin = new Intent(context, LoginActivity.class);
                                            intentLogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                            context.startActivity(intentLogin);

                                        } else {
                                            activity.runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show();
                                                }
                                            });
                                        }

                                    } else {
                                        String resultJSON = OperationDataJsonSerializer.serialize(response.data(), " ");
                                        Logger.d(resultJSON);
                                        FavoritePostMutation.FavoritePost favorite = response.data().favoritePost();
                                        Gson gson = new Gson();
                                        PostQuery.FollowingPost post = gson.fromJson(gson.toJson(favorite.post()), PostQuery.FollowingPost.class);
                                        Logger.d(post);
                                        DataClass newPost = new DataClass(R.drawable.more,R.color.Basic,R.drawable.comment,"Thích",R.drawable.heart, post);
                                        activity.runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                imgReact.setEnabled(true);
                                                dataClass.set(position, newPost);
                                                notifyItemChanged(position);
                                            }
                                        });
                                    }
                                }

                                @Override
                                public void onFailure(@NotNull ApolloException e) {

                                    Logger.e(e.getLocalizedMessage());
                                    Logger.e(e.getCause().toString());
                                    activity.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            imgReact.setEnabled(true);
                                            Toast.makeText(context, e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                }
                            });



                            break;
                        }
                        case R.drawable.heart_x: {
                            imgReact.setImageResource(R.drawable.heart);
                            dataClass.get(position).setReact(R.drawable.heart);
                            luotthich.setText(--favoritesCount+"");
                            requestUnFavoritePost = application.apolloClient()
                                                    .mutate(UnFavoritePostMutation
                                                            .builder()
                                                            .postId(postId)
                                                            .build());
                            requestUnFavoritePost.enqueue(new ApolloCall.Callback<UnFavoritePostMutation.Data>() {
                                @Override
                                public void onResponse(@NotNull Response<UnFavoritePostMutation.Data> response) {

                                    if(response.hasErrors()) {
                                        activity.runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                imgReact.setEnabled(true);
                                                imgReact.setImageResource(R.drawable.heart_x);
                                                dataClass.get(position).setReact(R.drawable.heart_x);
                                            }
                                        });
                                        String errorMessage = response.errors().get(0).message();
                                        Logger.e(errorMessage);
                                        if (errorMessage.contains("Authenticated Fail") ||  errorMessage.contains("jwt")) {
                                            Intent intentLogin = new Intent(context, LoginActivity.class);
                                            intentLogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                            context.startActivity(intentLogin);

                                        } else {
                                            activity.runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show();
                                                }
                                            });
                                        }

                                    } else {
                                        String resultJSON = OperationDataJsonSerializer.serialize(response.data(), " ");
                                        Logger.d(resultJSON);
                                        UnFavoritePostMutation.UnFavoritePost unfavorite = response.data().unFavoritePost();
                                        Gson gson = new Gson();
                                        PostQuery.FollowingPost post = gson.fromJson(gson.toJson(unfavorite.post()), PostQuery.FollowingPost.class);
                                        Logger.d(post);
                                        DataClass newPost = new DataClass(R.drawable.more,R.color.Basic,R.drawable.comment,"Thích",R.drawable.heart, post);
                                        activity.runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                imgReact.setEnabled(true);
                                                dataClass.set(position, newPost);
                                                notifyItemChanged(position);
                                            }
                                        });
                                    }
                                }

                                @Override
                                public void onFailure(@NotNull ApolloException e) {

                                    Logger.e(e.getLocalizedMessage());
                                    Logger.e(e.getCause().toString());
                                    activity.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            imgReact.setEnabled(true);
                                            Toast.makeText(context, e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                }
                            });
                            break;
                        }
                    }
                }

            });
            imgSet.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    BottomSheetDialog bt = new BottomSheetDialog(itemView.getContext(),R.style.bottomOption);
                    View bottomSheetView = LayoutInflater.from(itemView.getContext())
                            .inflate(
                                    R.layout.layout_bottom_chon,(LinearLayout)itemView.findViewById(R.id.bottomOption)
                            );
                    bt.setContentView(bottomSheetView);
                    bt.show();
                    EditText tx =(EditText) bt.findViewById(R.id.optiontext);
                    TextView pb =(TextView) bt.findViewById(R.id.Public);
                    TextView pr =(TextView)bt.findViewById(R.id.Private);
                    ColorStateList oldColors =  pb.getTextColors();
                    if(dataClass.get(position).mPost.published()==true)
                    {
                        pb.setTextColor(Color.parseColor("#0576ff"));
                        state = true;
                    }
                    if(dataClass.get(position).mPost.published()==false)
                    {
                        pr.setTextColor(Color.parseColor("#0576ff"));
                        state = false;
                    }
                    tx.setText(conTent.getText().toString());
                    bt.findViewById(R.id.XoaBai).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            RemoveItem(getAdapterPosition());
                            bt.dismiss();
                        }
                    });
                    bt.findViewById(R.id.suabai).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if(!conTent.getText().equals(tx.getText().toString()) && !tx.getText().toString().equals(""))
                            {
                                Context contextView = itemView.getContext();
                                Activity activity = (Activity) contextView;
                                requestUpdateTitlePost = application.apolloClient()
                                        .mutate(UpdateTitlePostMutation
                                                .builder().postId(idviet).title(tx.getText().toString())
                                                .build());
                                requestUpdateTitlePost.enqueue(new ApolloCall.Callback<UpdateTitlePostMutation.Data>() {
                                    @Override
                                    public void onResponse(@NotNull Response<UpdateTitlePostMutation.Data> response) {
                                        if(response.hasErrors()) {
                                            String errorMessage = response.errors().get(0).message();
                                            Logger.e(errorMessage);
                                            if (errorMessage.contains("Authenticated Fail") ||  errorMessage.contains("jwt")) {
                                                Intent intentLogin = new Intent(context, LoginActivity.class);
                                                intentLogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                context.startActivity(intentLogin);

                                            } else {
                                                Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show();
                                            }

                                        } else {
                                            String resultJSON = OperationDataJsonSerializer.serialize(response.data(), " ");
                                            Logger.d(resultJSON);
                                            UpdateTitlePostMutation.UpdatePost data = response.data().updatePost();
                                            Gson gson = new Gson();
                                            PostQuery.FollowingPost post = gson.fromJson(gson.toJson(data), PostQuery.FollowingPost.class);
                                            Logger.d(post);
                                            DataClass newPost = new DataClass(R.drawable.more,R.color.Basic,R.drawable.comment,"Thích",R.drawable.heart, post);
                                            activity.runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {

                                                    dataClass.set(position, newPost);
                                                    notifyItemChanged(position);
                                                }
                                            });
                                        }
                                    }

                                    @Override
                                    public void onFailure(@NotNull ApolloException e) {
                                        Logger.e(e.getLocalizedMessage());
                                        Logger.e(e.getCause().toString());
                                        Toast.makeText(context, e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                                    }
                                });
                                //dataClass.get(getAdapterPosition()).mPost.
                                bt.dismiss();
                            } else {
                                Toast.makeText(context,"Bài viết chưa cập nhật",Toast.LENGTH_LONG).show();
                            }

                            //Toast.makeText(context,""+dataClass.get(getAdapterPosition()).mPost.title(),Toast.LENGTH_LONG).show();
                        }
                    });
                    bt.findViewById(R.id.buttonPrivate).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            state = false;
                            pr.setTextColor(Color.parseColor("#0576ff"));
                            pb.setTextColor(oldColors);
                        }
                    });
                    bt.findViewById(R.id.buttonPublic).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            state = true;
                            pb.setTextColor(Color.parseColor("#0576ff"));
                            pr.setTextColor(oldColors);
                        }
                    });
                    bt.findViewById(R.id.suaquyen).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            boolean prePublished = dataClass.get(position).mPost.published();
                            Context contextView = itemView.getContext();
                            Activity activity = (Activity) contextView;
                            if(prePublished == state) {
                                Toast.makeText(context,"Bài viết chưa cập nhật",Toast.LENGTH_LONG).show();
                            } else {
                                bt.dismiss();
                                requestUpdatePrivacyPost = application.apolloClient()
                                        .mutate(UpdatePrivacyPostMutation
                                                .builder()
                                                .postId(idviet)
                                                .published(state)
                                                .build());
                                requestUpdatePrivacyPost.enqueue(new ApolloCall.Callback<UpdatePrivacyPostMutation.Data>() {
                                    @Override
                                    public void onResponse(@NotNull Response<UpdatePrivacyPostMutation.Data> response) {
                                        if(response.hasErrors()) {
                                            String errorMessage = response.errors().get(0).message();
                                            Logger.e(errorMessage);
                                            if (errorMessage.contains("Authenticated Fail") ||  errorMessage.contains("jwt")) {
                                                Intent intentLogin = new Intent(context, LoginActivity.class);
                                                intentLogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                context.startActivity(intentLogin);

                                            } else {
                                                activity.runOnUiThread(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show();
                                                    }
                                                });
                                            }

                                        } else {
                                            String resultJSON = OperationDataJsonSerializer.serialize(response.data(), " ");
                                            Logger.d(resultJSON);
                                            UpdatePrivacyPostMutation.UpdatePost data = response.data().updatePost();
                                            Gson gson = new Gson();
                                            PostQuery.FollowingPost post = gson.fromJson(gson.toJson(data), PostQuery.FollowingPost.class);
                                            Logger.d(post);
                                            DataClass newPost = new DataClass(R.drawable.more,R.color.Basic,R.drawable.comment,"Thích",R.drawable.heart, post);
                                            activity.runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    dataClass.set(position, newPost);
                                                    notifyItemChanged(position);
                                                }
                                            });
                                        }
                                    }

                                    @Override
                                    public void onFailure(@NotNull ApolloException e) {
                                        Logger.e(e.getLocalizedMessage());
                                        Logger.e(e.getCause().toString());
                                        activity.runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                Toast.makeText(context, e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                                            }
                                        });
                                    }
                                });
                            }
                        }
                    });


                }
            });
        }


    }

}
