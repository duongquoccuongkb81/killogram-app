package com.example.kilogramapp.Utils.GridImageComponent;

public class GridViewItem {
    private String postId;
    private String postImageUrl;

    public GridViewItem(String postId, String postImageUrl) {
        this.postId = postId;
        this.postImageUrl = postImageUrl;
    }

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public String getPostImageUrl() {
        return postImageUrl;
    }

    public void setPostImageUrl(String postImageUrl) {
        this.postImageUrl = postImageUrl;
    }
}
