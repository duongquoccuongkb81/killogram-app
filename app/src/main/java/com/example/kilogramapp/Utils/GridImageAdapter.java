package com.example.kilogramapp.Utils;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView.ViewHolder;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.example.kilogramapp.Main.Comment.extend;
import com.example.kilogramapp.R;
import com.example.kilogramapp.Utils.GridImageComponent.GridViewItem;

import java.util.ArrayList;

public class GridImageAdapter extends ArrayAdapter<GridViewItem> {
    private static final String TAG = "GridImageAdapter";
    private Context mContext;
    private LayoutInflater mInflater;
    private int layoutResource;
    private String mAppend;
    private ArrayList<GridViewItem> posts;
    public static final String CHUYENID ="chuyenID";

    public GridImageAdapter(Context context, int layoutResource, String append, ArrayList<GridViewItem> posts) {
        super(context, layoutResource, posts);
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mContext = context;
        this.layoutResource = layoutResource;
        mAppend = append;
        this.posts = posts;
    }

    private static class ViewHolder {
        SquareImageView mImage;
        String postId;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        // viewHolder build pattern (Similar to recyleview)
        final ViewHolder viewHolder;

        if (convertView == null) {
            convertView = mInflater.inflate(layoutResource, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.mImage = (SquareImageView) convertView.findViewById(R.id.gridImageView);
            viewHolder.postId = getItem(position).getPostId();
            convertView.setTag(viewHolder);
            viewHolder.mImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent a = new Intent(mContext, extend.class);
                    a.putExtra(CHUYENID,viewHolder.postId);
                    mContext.startActivity(a);
                }
            });
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        String imgURL = getItem(position).getPostImageUrl();
        Glide.with(mContext)
                .load(mAppend+imgURL)
                .placeholder(R.drawable.ic_loadmore)
                .fitCenter()
                .apply(RequestOptions.overrideOf(Target.SIZE_ORIGINAL))
                .into(viewHolder.mImage);
        /*
        ImageLoader imageLoader = ImageLoader.getInstance();
        imageLoader.displayImage(mAppend + imgURL, viewHolder.mImage, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {
                if (viewHolder.mProgressBar != null) {
                    viewHolder.mProgressBar.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                if (viewHolder.mProgressBar != null) {
                    viewHolder.mProgressBar.setVisibility(View.GONE);
                }
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                if (viewHolder.mProgressBar != null) {
                    viewHolder.mProgressBar.setVisibility(View.GONE);
                }
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {
                if (viewHolder.mProgressBar != null) {
                    viewHolder.mProgressBar.setVisibility(View.GONE);
                }
            }
        });

         */
        return convertView;
    }
}
