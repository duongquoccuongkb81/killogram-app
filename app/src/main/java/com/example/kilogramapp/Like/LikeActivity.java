package com.example.kilogramapp.Like;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.ApolloCallback;
import com.apollographql.apollo.api.OperationDataJsonSerializer;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;
import com.apollographql.apollo.fetcher.ApolloResponseFetchers;
import com.example.kilogramapp.Application.GlobalApplication;
import com.example.kilogramapp.ConversationByIDQuery;
import com.example.kilogramapp.Login.LoginActivity;
import com.example.kilogramapp.MyFavoritesQuery;
import com.example.kilogramapp.PostQuery;
import com.example.kilogramapp.R;
import com.example.kilogramapp.RecycleViewComponent.DataLikeFollow;
import com.example.kilogramapp.RecycleViewComponent.DataNotify;
import com.example.kilogramapp.RecycleViewComponent.NoiDungChat;
import com.example.kilogramapp.SendMessageMutation;
import com.example.kilogramapp.Utils.BottomNavigationViewHelper;
import com.example.kilogramapp.Utils.LFAdapter;
import com.example.kilogramapp.Utils.NotifyAdapter;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;
import com.orhanobut.logger.Logger;

import org.jetbrains.annotations.NotNull;
import org.w3c.dom.Text;

import java.util.ArrayList;

public class LikeActivity extends AppCompatActivity {
    private static final String TAG = "LikeActivity";
    private static final int ACTIVITY_NUM = 3;
    private TextView toolbarText;
    private ImageView backArrow;

    private Context mContext = LikeActivity.this;
    ArrayList<DataNotify> arrayFavorites = new ArrayList<DataNotify>();
    RecyclerView rwNo;
    NotifyAdapter notifyAdapter;
    private String userId;
    private GlobalApplication application;
    private Handler uiHandler = new Handler(Looper.getMainLooper());
    private ApolloCall<MyFavoritesQuery.Data> requestMyFavorites;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notify);
        TextView textView = (TextView)findViewById(R.id.usecase);
        backArrow = (ImageView)findViewById(R.id.back_lf);
        backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        textView.setText("Thông báo");
        Log.d(TAG, "onCreate: started");
        rwNo= (RecyclerView)findViewById(R.id.rwnotify);
        rwNo.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        rwNo.setLayoutManager(layoutManager);
        application = (GlobalApplication) getApplication();
        application.createSharedPreferences(mContext);
        String token = application.getToken();
        if(token == null) {
            Toast.makeText(mContext, "Authenticated Fail", Toast.LENGTH_SHORT).show();
            Intent intentLogin = new Intent(mContext, LoginActivity.class);
            intentLogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intentLogin);
        }
        application.createApolloClient(token);
        userId = application.getUserId();
        populateData();
        initView();
        setupBottomNavigationView();
    }
    private ApolloCall.Callback<MyFavoritesQuery.Data> requestMyConversationCallBack
            = new ApolloCallback<>(new ApolloCall.Callback<MyFavoritesQuery.Data>() {
        @Override
        public void onResponse(@NotNull Response<MyFavoritesQuery.Data> response) {
            if(response.hasErrors()) {
                String errorMessage = response.errors().get(0).message();
                Logger.e(errorMessage);
                if (errorMessage.contains("Authenticated Fail") ||  errorMessage.contains("jwt")) {
                    application.removeStoredValue();
                    Intent intentLogin = new Intent(mContext, LoginActivity.class);
                    intentLogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intentLogin);
                } else {
                    Toast.makeText(mContext, errorMessage, Toast.LENGTH_SHORT).show();
                }
            } else {
                assert response.data() != null;
                String dataJSON = OperationDataJsonSerializer.serialize(response.data(), " ");
                Logger.d(dataJSON);
                response.data().myFavorites().forEach(myFavorite -> {
                    DataNotify favorite = new DataNotify(myFavorite);
                    arrayFavorites.add(favorite);
                });
                initView();
            }
        }
        @Override
        public void onFailure(@NotNull ApolloException e) {
            Logger.e(e.getLocalizedMessage());
            Logger.e(String.valueOf(e.getCause()));
        }
    }, uiHandler);
    private void populateData() {
        requestMyFavorites = application.apolloClient()
                .query(MyFavoritesQuery.builder().build()).responseFetcher(ApolloResponseFetchers.NETWORK_FIRST);
        requestMyFavorites.enqueue(requestMyConversationCallBack);
    }
    public void initView(){
        notifyAdapter= new NotifyAdapter(arrayFavorites,mContext);
        rwNo.setAdapter(notifyAdapter);
    }
    private void setupBottomNavigationView() {
        Log.d(TAG, "setupBottomNavigationView: setting up");
        BottomNavigationViewEx bnve = (BottomNavigationViewEx) findViewById(R.id.bottomNavViewBar);
        BottomNavigationViewHelper.setupBottomNavigationView(bnve);
        BottomNavigationViewHelper.enableNavigation(mContext, bnve);
        Menu menu = bnve.getMenu();
        MenuItem menuItem = menu.getItem(ACTIVITY_NUM);
        menuItem.setChecked(true);
    }
}
