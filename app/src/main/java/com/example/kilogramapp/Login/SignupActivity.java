package com.example.kilogramapp.Login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.ApolloCallback;
import com.apollographql.apollo.api.OperationDataJsonSerializer;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;
import com.example.kilogramapp.Application.GlobalApplication;
import com.example.kilogramapp.CreateUserMutation;
import com.example.kilogramapp.LoginMutation;
import com.example.kilogramapp.Main.MainActivity;
import com.example.kilogramapp.R;
import com.example.kilogramapp.type.CreateUserInput;
import com.example.kilogramapp.type.GenderType;
import com.orhanobut.logger.Logger;

import org.jetbrains.annotations.NotNull;

public class SignupActivity extends AppCompatActivity {
    private static final String TAG = "SignupActivity";
    private ImageView mBtnBack;
    private EditText mInput_Name;
    private EditText mInput_UserName;
    private EditText mInput_Email;
    private EditText mInput_Age;
    private EditText mInput_Password;
    private TextView mTextViewGotoSignIn;
    private Button mBtnSignUp;
    private RadioGroup mRadioGroupGender;
    private String gender;
    private ProgressBar mProgressBar;
    private Context mContext = SignupActivity.this;
    private GlobalApplication application;
    private Handler uiHandler = new Handler(Looper.getMainLooper());
    private ApolloCall<CreateUserMutation.Data> requestCreateUser;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        Log.d(TAG, "onCreate: started.");
        mBtnBack = findViewById(R.id.back_arrow);
        mInput_Name = findViewById(R.id.input_name);
        mInput_UserName = findViewById(R.id.input_usename);
        mInput_Age = findViewById(R.id.input_age);
        mInput_Email = findViewById(R.id.input_email);
        mInput_Password = findViewById(R.id.input_password);
        mTextViewGotoSignIn = findViewById(R.id.tv_navigating_signin);
        mBtnSignUp = findViewById(R.id.btn_signup);
        mRadioGroupGender = findViewById(R.id.genderRadioGroup);
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);
        mProgressBar.setVisibility(View.GONE);
        application = (GlobalApplication) getApplication();
        application.createSharedPreferences(mContext);
        application.createApolloClientWithoutToken();
        Initial();
        setUpSignUp();
    }

    private void Initial() {
        mBtnBack.setOnClickListener(v -> {
            finish();
        });
        mTextViewGotoSignIn.setOnClickListener(v -> {
            Intent intent = new Intent(mContext, LoginActivity.class);
            startActivity(intent);
        });
        mRadioGroupGender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.maleRadioButton:
                        gender = "Male";
                        break;
                    case R.id.femaleRadioButton:
                        gender = "Female";
                        break;
                }
            }
        });
    }
    private ApolloCall.Callback<CreateUserMutation.Data> requestRegisterCallBack
            = new ApolloCallback<>(new ApolloCall.Callback<CreateUserMutation.Data>() {
        @Override
        public void onResponse(@NotNull Response<CreateUserMutation.Data> response) {
            mProgressBar.setVisibility(View.GONE);
            mBtnSignUp.setEnabled(true);
            if(response.hasErrors()) {
                String errorMessage = response.errors().get(0).message();
                Logger.e(errorMessage);
                Toast.makeText(mContext, errorMessage, Toast.LENGTH_SHORT).show();

            } else {
                String resultJSON = OperationDataJsonSerializer.serialize(response.data(), " ");
                Logger.d(resultJSON);
                String token = response.data().createUser().token();
                String userId = response.data().createUser().user().id();
                application.storeToken(token);
                application.storeUserId(userId);
                Intent intentMain= new Intent(mContext, MainActivity.class);
                startActivity(intentMain);
            }
        }

        @Override
        public void onFailure(@NotNull ApolloException e) {
            Logger.e(e.getLocalizedMessage());
            Logger.e(e.getCause().toString());
            mProgressBar.setVisibility(View.GONE);
            mBtnSignUp.setEnabled(true);
            Toast.makeText(mContext, e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
        }
    }, uiHandler);
    private void setUpSignUp () {
        mBtnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = mInput_Name.getText().toString();
                String username = mInput_UserName.getText().toString();
                String email = mInput_Email.getText().toString();
                String password = mInput_Password.getText().toString();
                int age;
                if (isStringNull(mInput_Age.getText().toString())) {
                    age = 0;
                } else {
                    age = Integer.parseInt(mInput_Age.getText().toString());
                }
                if (isStringNull(name)) {
                    Toast.makeText(mContext, "Vui lòng nhập name", Toast.LENGTH_SHORT).show();
                    return;
                } else if (isStringNull(username)) {
                    Toast.makeText(mContext, "Vui lòng nhập username", Toast.LENGTH_SHORT).show();
                    return;
                } else if (isStringNull(email)) {
                    Toast.makeText(mContext, "Vui lòng nhập email", Toast.LENGTH_SHORT).show();
                    return;
                } else if (isStringNull(password)) {
                    Toast.makeText(mContext, "Vui lòng nhập password", Toast.LENGTH_SHORT).show();
                    return;
                }
                mProgressBar.setVisibility(View.VISIBLE);
                mBtnSignUp.setEnabled(false);
                GenderType type = null;
                if (gender == null) {
                   type = GenderType.MALE;
                } else if (gender.equals("Male")) {
                    type = GenderType.MALE;
                } else if (gender.equals("Female")) {
                    type = GenderType.FEMALE;
                }

                CreateUserInput dataInput = CreateUserInput.builder()
                        .email(email)
                        .username(username)
                        .age(age)
                        .gender(type)
                        .password(password)
                        .build();
                requestCreateUser = application.apolloClient()
                        .mutate(CreateUserMutation
                                .builder()
                                .data(dataInput)
                                .name(name)
                                .build());
                requestCreateUser.enqueue(requestRegisterCallBack);

            }
        });
    }
    private boolean isStringNull(String string){
        Log.d(TAG, "isStringNull: checking string if null");

        if (string.equals("")){
            return true;
        }
        return false;
    }
}
