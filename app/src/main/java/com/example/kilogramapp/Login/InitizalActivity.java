package com.example.kilogramapp.Login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.ApolloCallback;
import com.apollographql.apollo.api.OperationDataJsonSerializer;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;
import com.apollographql.apollo.fetcher.ApolloResponseFetchers;
import com.example.kilogramapp.Application.GlobalApplication;
import com.example.kilogramapp.CheckLoginQuery;
import com.example.kilogramapp.Main.MainActivity;
import com.example.kilogramapp.PostQuery;
import com.example.kilogramapp.R;
import com.example.kilogramapp.RecycleViewComponent.DataClass;
import com.orhanobut.logger.AndroidLogAdapter;
import com.orhanobut.logger.Logger;

import org.jetbrains.annotations.NotNull;

import java.util.function.Consumer;

public class InitizalActivity extends AppCompatActivity {
    private static final String TAG = "InitizalActivity";
    private Context mContext = InitizalActivity.this;
    private GlobalApplication application;
    private Handler uiHandler = new Handler(Looper.getMainLooper());
    private ApolloCall<CheckLoginQuery.Data> requestUserId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loadapp);
        Logger.clearLogAdapters();
        Logger.addLogAdapter(new AndroidLogAdapter());
        application = (GlobalApplication) getApplication();
        application.createSharedPreferences(mContext);
        String token = application.getToken();
        if (token == null) {
            Logger.d("Token Not Found");
            Intent intentLogin = new Intent(mContext, LoginActivity.class);
            startActivity(intentLogin);
        } else {
            Logger.d("Token was Found");
            application.createApolloClient(token);
            checkAuth();
        }
    }
    private ApolloCall.Callback<CheckLoginQuery.Data> requestUserIdCallBack
            = new ApolloCallback<>(new ApolloCall.Callback<CheckLoginQuery.Data>() {
        @Override
        public void onResponse(@NotNull Response<CheckLoginQuery.Data> response) {
            if(response.hasErrors()) {
                String errorMessage = response.errors().get(0).message();
                Logger.e(errorMessage);
                if (errorMessage.contains("Authenticated Fail") ||  errorMessage.contains("jwt")) {
                    application.removeStoredValue();
                    Intent intentLogin = new Intent(mContext, LoginActivity.class);
                    intentLogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intentLogin);
                }
            } else {
                String resultJSON = OperationDataJsonSerializer.serialize(response.data(), " ");
                Logger.d(resultJSON);
                String userId = response.data().checkLogin();
                Logger.d("userId from request" + userId);
                Logger.d("userId from stored" + application.getUserId());
                if (!userId.equals(application.getUserId())){
                    Toast.makeText(mContext, "Authoziation Faile", Toast.LENGTH_SHORT).show();
                    application.removeStoredValue();
                    Intent intentLogin = new Intent(mContext, LoginActivity.class);
                    intentLogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intentLogin);
                }
                Intent intentMain = new Intent(mContext, MainActivity.class);
                intentMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intentMain);
            }
        }

        @Override
        public void onFailure(@NotNull ApolloException e) {
            Logger.e(e.getLocalizedMessage());
            Logger.e(e.getCause().toString());
            //Toast.makeText(mContext, e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
        }
    }, uiHandler);

    private void checkAuth() {
        requestUserId = application.apolloClient().query(CheckLoginQuery
                .builder()
                .build())
                .responseFetcher(ApolloResponseFetchers.NETWORK_FIRST);
        requestUserId.enqueue(requestUserIdCallBack);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (requestUserId != null) {
            requestUserId.cancel();
        }
    }
}
