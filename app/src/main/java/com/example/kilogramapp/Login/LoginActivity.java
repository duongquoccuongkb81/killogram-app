package com.example.kilogramapp.Login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.ApolloCallback;
import com.apollographql.apollo.api.OperationDataJsonSerializer;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;
import com.example.kilogramapp.Application.GlobalApplication;
import com.example.kilogramapp.LoginMutation;
import com.example.kilogramapp.Main.MainActivity;
import com.example.kilogramapp.R;
import com.example.kilogramapp.type.InputLogin;
import com.orhanobut.logger.AndroidLogAdapter;
import com.orhanobut.logger.Logger;

import org.jetbrains.annotations.NotNull;

public class LoginActivity extends AppCompatActivity {
    private static final String TAG = "LoginActivity";

    private Context mContext = LoginActivity.this;;
    private ProgressBar mProgressBar;
    private EditText mEmail, mPassword;
    private Button mButtonLogin;
    private TextView mTextViewGoToSignUp;
    private GlobalApplication application;
    private Handler uiHandler = new Handler(Looper.getMainLooper());
    private ApolloCall<LoginMutation.Data> requestLogin;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Logger.clearLogAdapters();
        Logger.addLogAdapter(new AndroidLogAdapter());
        application = (GlobalApplication) getApplication();
        application.createApolloClientWithoutToken();
        application.createSharedPreferences(mContext);
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);
        mEmail = (EditText) findViewById(R.id.input_email);
        mPassword = (EditText) findViewById(R.id.textinputpassword);
        mButtonLogin = (Button) findViewById(R.id.btn_login);
        mTextViewGoToSignUp = findViewById(R.id.text_navigating_signup);
        Log.d(TAG, "onCreate: started.");
        mProgressBar.setVisibility(View.GONE);
        mTextViewGoToSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentSignUp = new Intent(mContext, SignupActivity.class);
                startActivity(intentSignUp);
            }
        });
        setBtnLoginEvent();

    }
    
    private boolean isStringNull(String string){
        Log.d(TAG, "isStringNull: checking string if null");

        if (string.equals("")){
            return true;
        }
        return false;
    }

    private ApolloCall.Callback<LoginMutation.Data> requestLoginCallBack
            = new ApolloCallback<>(new ApolloCall.Callback<LoginMutation.Data>() {
        @Override
        public void onResponse(@NotNull Response<LoginMutation.Data> response) {
            mProgressBar.setVisibility(View.GONE);
            mButtonLogin.setEnabled(true);
            if(response.hasErrors()) {
                String errorMessage = response.errors().get(0).message();
                Logger.e(errorMessage);
                if (errorMessage.contains("Unable to login")) {
                    Toast.makeText(mContext, "Tài khoản hoặc mật khẩu không đúng!", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(mContext, errorMessage, Toast.LENGTH_SHORT).show();
                }

            } else {
                String resultJSON = OperationDataJsonSerializer.serialize(response.data(), " ");
                Logger.d(resultJSON);
                String token = response.data().login().token();
                String userId = response.data().login().user().id();
                application.storeToken(token);
                application.storeUserId(userId);
                Intent intentMain= new Intent(mContext, MainActivity.class);
                startActivity(intentMain);
            }
        }

        @Override
        public void onFailure(@NotNull ApolloException e) {
            mProgressBar.setVisibility(View.GONE);
            mButtonLogin.setEnabled(true);
            Logger.e(e.getLocalizedMessage());
            Logger.e(e.getCause().toString());
            Toast.makeText(mContext, e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
        }
    }, uiHandler);
    //khởi tạo nút log in
    private void setBtnLoginEvent() {
        mButtonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Logger.d("Click Button");
                String emailValue = mEmail.getText().toString();
                String passwordValue = mPassword.getText().toString();
                if(isStringNull(emailValue) || isStringNull(passwordValue)) {
                    Logger.d("Email, Password is required");
                    Toast.makeText(mContext, "Email, Password is required",Toast.LENGTH_SHORT).show();
                } else {
                    View view = getCurrentFocus();
                    if (view != null) {
                        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    }
                    mProgressBar.setVisibility(View.VISIBLE);
                    mButtonLogin.setEnabled(false);
                    Logger.d("Waiting");
                    InputLogin input = InputLogin.builder()
                            .email(emailValue)
                            .password(passwordValue)
                            .build();
                    requestLogin = application.apolloClient().mutate(LoginMutation
                            .builder()
                            .data(input).build());
                    requestLogin.enqueue(requestLoginCallBack);

                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (requestLogin != null) {
            requestLogin.cancel();
        }
    }
}


