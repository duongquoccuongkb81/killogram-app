package com.example.kilogramapp.Main;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.api.OperationDataJsonSerializer;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;
import com.example.kilogramapp.Application.GlobalApplication;
import com.example.kilogramapp.GetMyProfileQuery;
import com.example.kilogramapp.Login.LoginActivity;
import com.example.kilogramapp.R;
import com.example.kilogramapp.Utils.BottomNavigationViewHelper;
import com.example.kilogramapp.Utils.SectionPagerAdapter;
import com.google.android.material.tabs.TabLayout;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;
import com.orhanobut.logger.AndroidLogAdapter;
import com.orhanobut.logger.Logger;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

public class MainActivity extends AppCompatActivity {
    //logt
    private static final String TAG = "MainActivity";
    private static final int ACTIVITY_NUM = 0;
    private Context mContext = MainActivity.this;
    private GlobalApplication application;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //logd
        Log.d(TAG, "onCreate: starting");
        Logger.clearLogAdapters();
        Logger.addLogAdapter(new AndroidLogAdapter());

        setupBottomNavigationView();
        setupViewPager();
        application = (GlobalApplication) getApplication();
        application.createSharedPreferences(mContext);
//        String token = application.getToken();
//        if(token == null) {
//            Toast.makeText(mContext, "Authenticated Fail", Toast.LENGTH_SHORT).show();
//            Intent intentLogin = new Intent(mContext, LoginActivity.class);
//            startActivity(intentLogin);
//        }
//        application.createApolloClient(token);
    }


    /**
     * Bottom Navigation View  setup
     */
    private void setupBottomNavigationView() {
        Log.d(TAG, "setupBottomNavigationView: setting up");
        BottomNavigationViewEx bnve = (BottomNavigationViewEx) findViewById(R.id.bottomNavViewBar);
        BottomNavigationViewHelper.setupBottomNavigationView(bnve);
        BottomNavigationViewHelper.enableNavigation(mContext, bnve);
        Menu menu = bnve.getMenu();
        MenuItem menuItem = menu.getItem(ACTIVITY_NUM);
        menuItem.setChecked(true);

    }

    private void setupViewPager() {
        Log.d(TAG, "setupViewPager: setting up");
        SectionPagerAdapter adapter = new SectionPagerAdapter(getSupportFragmentManager());
        //adapter.addFragment(new CameraFragment());
        adapter.addFragment(new MainFragment());
        adapter.addFragment(new MessageFragment());
        ViewPager viewPager = (ViewPager) findViewById(R.id.container);
        viewPager.setAdapter(adapter);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        //tabLayout.getTabAt(0).setIcon(R.drawable.ic_camera);
        tabLayout.getTabAt(0).setIcon(R.drawable.ic_newsfeed);
        tabLayout.getTabAt(1).setIcon(R.drawable.ic_chatfeed);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy: Main Activity Destroy");
    }
}
