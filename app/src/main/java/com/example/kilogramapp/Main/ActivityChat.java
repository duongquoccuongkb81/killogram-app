package com.example.kilogramapp.Main;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;

import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.ApolloCallback;
import com.apollographql.apollo.ApolloSubscriptionCall;
import com.apollographql.apollo.api.OperationDataJsonSerializer;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;
import com.apollographql.apollo.fetcher.ApolloResponseFetchers;
import com.apollographql.apollo.rx2.Rx2Apollo;
import com.example.kilogramapp.Application.GlobalApplication;
import com.example.kilogramapp.CommentSubscription;
import com.example.kilogramapp.ConversationByIDQuery;
import com.example.kilogramapp.CreateCommentMutation;
import com.example.kilogramapp.FindPostByIdQuery;
import com.example.kilogramapp.GlobalConversationSubscription;
import com.example.kilogramapp.Login.LoginActivity;
import com.example.kilogramapp.MessageSubscription;
import com.example.kilogramapp.MyConversationsQuery;
import com.example.kilogramapp.Profile.ViewProfileActivity;
import com.example.kilogramapp.R;
import com.example.kilogramapp.RecycleViewComponent.DataClassListChat;
import com.example.kilogramapp.RecycleViewComponent.DataCmt;
import com.example.kilogramapp.RecycleViewComponent.NoiDungChat;
import com.example.kilogramapp.SendMessageMutation;
import com.example.kilogramapp.Utils.BinhLuanAdapter;
import com.example.kilogramapp.Utils.ChatAdapter;
import com.example.kilogramapp.Utils.PostAdapter;
import com.example.kilogramapp.type.SendMessageInput;
import com.google.gson.Gson;
import com.orhanobut.logger.Logger;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.function.Consumer;

import de.hdodenhof.circleimageview.CircleImageView;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subscribers.DisposableSubscriber;

public class ActivityChat extends AppCompatActivity {
    private static final String TAG = "ActivityChat";
    private static final String CHUYENID ="chuyenID";
    private ArrayList<NoiDungChat> nd = new ArrayList<>();
    private RecyclerView rwChat;
    private ChatAdapter chatAdapter;
    private NestedScrollView nestedScrollView;
    private Context mContext = ActivityChat.this;
    private EditText editText;
    private ImageView send;
    private ImageView back;
    private CircleImageView avt;
    private TextView withusername;
    private GlobalApplication application;
    private Handler uiHandler = new Handler(Looper.getMainLooper());
    private ApolloCall<ConversationByIDQuery.Data> requestConversationById;
    private ApolloCall<SendMessageMutation.Data> requestSendMessage;
    private String userId;
    private String conversationId;
    private String withUserId;
    private ConversationByIDQuery.Conversation mConversation;
    private final CompositeDisposable disposables = new CompositeDisposable();
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chat_main);
        avt =  (CircleImageView)findViewById(R.id.tamHinh3);
        withusername = (TextView)findViewById(R.id.textView3);
        rwChat = (RecyclerView)findViewById(R.id.rwChat);
        nestedScrollView = findViewById(R.id.nestedScrollView);
        rwChat.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        //layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rwChat.setLayoutManager(layoutManager);
        send = (ImageView)findViewById(R.id.sendcmt);
        editText = (EditText)findViewById(R.id.edittext);
        back = (ImageView)findViewById(R.id.back_arrow);
        application = (GlobalApplication) getApplication();
        application.createSharedPreferences(mContext);
        String token = application.getToken();
        if(token == null) {
            Toast.makeText(mContext, "Authenticated Fail", Toast.LENGTH_SHORT).show();
            Intent intentLogin = new Intent(mContext, LoginActivity.class);
            intentLogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intentLogin);
        }
        application.createApolloClient(token);
        userId = application.getUserId();
        conversationId = getIntent().getStringExtra(CHUYENID);
        initialUI();
        if (conversationId != null) {
            populateData();
            subscriptionMessage();
        }


    }

    public void insertItem(NoiDungChat data) {
        int position = nd.size();
        nd.add(position, data);
        chatAdapter.notifyItemInserted(position);
    }

    private ApolloCall.Callback<ConversationByIDQuery.Data> requestMyConversationCallBack
            = new ApolloCallback<>(new ApolloCall.Callback<ConversationByIDQuery.Data>() {
        @Override
        public void onResponse(@NotNull Response<ConversationByIDQuery.Data> response) {
            if(response.hasErrors()) {
                String errorMessage = response.errors().get(0).message();
                Logger.e(errorMessage);
                if (errorMessage.contains("Authenticated Fail") ||  errorMessage.contains("jwt")) {
                    application.removeStoredValue();
                    Intent intentLogin = new Intent(mContext, LoginActivity.class);
                    intentLogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intentLogin);
                } else {
                    Toast.makeText(mContext, errorMessage, Toast.LENGTH_SHORT).show();
                }
            } else {
                assert response.data() != null;
                String dataJSON = OperationDataJsonSerializer.serialize(response.data(), " ");
                Logger.d(dataJSON);
                mConversation = response.data().conversation();
                withUserId = mConversation.withUser().id();
                mConversation.messages().forEach(message -> {
                    NoiDungChat msg = new NoiDungChat(message);
                    nd.add(msg);
                });
                setupUI();
                initView();
            }
        }
        @Override
        public void onFailure(@NotNull ApolloException e) {
            Logger.e(e.getLocalizedMessage());
            Logger.e(String.valueOf(e.getCause()));
        }
    }, uiHandler);

    private ApolloCall.Callback<SendMessageMutation.Data> requestSendMessageCallBack
            = new ApolloCallback<>(new ApolloCall.Callback<SendMessageMutation.Data>() {
        @Override
        public void onResponse(@NotNull Response<SendMessageMutation.Data> response) {
            send.setEnabled(true);
            if(response.hasErrors()) {
                String errorMessage = response.errors().get(0).message();
                Logger.e(errorMessage);
                if (errorMessage.contains("Authenticated Fail") ||  errorMessage.contains("jwt")) {
                    application.removeStoredValue();
                    Intent intentLogin = new Intent(mContext, LoginActivity.class);
                    intentLogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intentLogin);
                } else if (errorMessage.contains("Receiver not found")) {
                    Toast.makeText(mContext, errorMessage, Toast.LENGTH_SHORT).show();
                    finish();
                } else {
                    Toast.makeText(mContext, errorMessage, Toast.LENGTH_SHORT).show();
                }
            } else {
                assert response.data() != null;
                String dataJSON = OperationDataJsonSerializer.serialize(response.data(), " ");
                Logger.d(dataJSON);
                SendMessageMutation.SendMessage messageData = response.data().sendMessage();
                Gson gson = new Gson();
                ConversationByIDQuery.Message msg = gson.fromJson(gson.toJson(messageData), ConversationByIDQuery.Message.class);
                Logger.d(msg);
                NoiDungChat  newMessage  = new NoiDungChat(msg);
                insertItem(newMessage);
//                float y = rwChat.getY() + rwChat.getChildAt(nd.size() - 2).getY();
//                nestedScrollView.smoothScrollTo(0, (int) y);
                rwChat.post(() -> {
                    nestedScrollView.post(new Runnable() {
                        @Override
                        public void run() {
                            nestedScrollView.fullScroll(View.FOCUS_DOWN);
                        }
                    });
                    rwChat.smoothScrollToPosition(nd.size() -1);
                });
            }
        }
        @Override
        public void onFailure(@NotNull ApolloException e) {
            send.setEnabled(true);
            Toast.makeText(mContext, e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            Logger.e(e.getLocalizedMessage());
            Logger.e(String.valueOf(e.getCause()));
        }
    }, uiHandler);
    private void populateData() {
        requestConversationById = application.apolloClient()
                .query(ConversationByIDQuery
                        .builder()
                        .conversationId(conversationId)
                        .build()).responseFetcher(ApolloResponseFetchers.NETWORK_FIRST);
        requestConversationById.enqueue(requestMyConversationCallBack);

    }
    private void initialUI() {
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }
    private void setupUI() {
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String content = editText.getText().toString();
                if(!isStringNull(content)) {
                    send.setEnabled(false);
                    editText.setText("");
                    SendMessageInput input = SendMessageInput.builder()
                            .userId(withUserId)
                            .text(content)
                            .build();
                    requestSendMessage =  application.apolloClient()
                            .mutate(SendMessageMutation
                                    .builder()
                                    .data(input)
                                    .build());
                    requestSendMessage.enqueue(requestSendMessageCallBack);
                }
            }
        });
        String url = getResources().getString(R.string.default_avatar);
        if (mConversation.withUser().profile().avatar() != null) {
            url = mConversation.withUser().profile().avatar().path();
        }
        Picasso.get().load(url).into(avt);
        withusername.setText(mConversation.withUser().username());
        withusername.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent  = new Intent(v.getContext(), ViewProfileActivity.class);
                intent.putExtra("userId", mConversation.withUser().id());
                v.getContext().startActivity(intent);
            }
        });
        avt.setVisibility(View.VISIBLE);
        withusername.setVisibility(View.VISIBLE);
    }
    public void initView(){

        //DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this,layoutManager.getOrientation());
        //recyclerView.addItemDecoration(dividerItemDecoration);
        chatAdapter= new ChatAdapter(nd,mContext);
        rwChat.setAdapter(chatAdapter);
        if (nd.size() > 0) {
            rwChat.post(() -> {
                float y = rwChat.getY() + rwChat.getChildAt(nd.size() - 1).getY();
                nestedScrollView.smoothScrollTo(0, (int) y);
                rwChat.smoothScrollToPosition(nd.size() -1);
            });
        }
    }
    private boolean isStringNull(String string){
        Log.d(TAG, "isStringNull: checking string if null");

        if (string.equals("")){
            return true;
        }
        return false;
    }
    private void subscriptionMessage() {
        ApolloSubscriptionCall<MessageSubscription.Data> subscriptionCall = application.apolloClient()
                .subscribe(MessageSubscription.builder().conversationId(conversationId).build());
        disposables.add(Rx2Apollo.from(subscriptionCall)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSubscriber<Response<MessageSubscription.Data>>() {
                    @Override
                    public void onNext(Response<MessageSubscription.Data> dataResponse) {
                        Logger.d("Response Subscription Handler");
                        if (dataResponse.hasErrors()) {
                            String errorMessage = dataResponse.errors().get(0).message();
                            Logger.e(errorMessage);
                            if (errorMessage.contains("Authenticated Fail") || errorMessage.contains("jwt")) {
                                application.removeStoredValue();
                                Intent intentLogin = new Intent(mContext, LoginActivity.class);
                                intentLogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intentLogin);
                            }
                        } else {
                            String dataJSON = OperationDataJsonSerializer.serialize(dataResponse.data(), " ");
                            Logger.d(dataJSON);
                            MessageSubscription.Message data = dataResponse.data().message();
                            Gson gson = new Gson();
                            ConversationByIDQuery.Message message = gson.fromJson(gson.toJson(data.data()), ConversationByIDQuery.Message.class);
                            Logger.d(message);
                            //post.
                            if (data.mutation().rawValue().equals("CREATED")) {
                                if(!data.data().sender().id().equals(userId)) {
                                    NoiDungChat newMessage = new NoiDungChat(message);
                                    int position = nd.size();
                                    nd.add(position, newMessage);
                                    chatAdapter.notifyItemInserted(position);
                                    LinearLayoutManager linearLayoutManager = (LinearLayoutManager) rwChat.getLayoutManager();
                                    int lastView = linearLayoutManager.findLastVisibleItemPosition();
                                    Logger.d(nd.size() -1);
                                    if (lastView >= nd.size() - 3) {
                                        nestedScrollView.post(new Runnable() {
                                            @Override
                                            public void run() {
                                                nestedScrollView.fullScroll(View.FOCUS_DOWN);
                                            }
                                        });
                                        rwChat.smoothScrollToPosition(nd.size() - 1);
                                    }
                                }
                            }
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        Logger.e(t.getMessage());
                        Logger.e(t.getCause().toString());
                        Toast.makeText(mContext, t.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onComplete() {
                        Logger.d("Subscription Completed");
                        Toast.makeText(mContext, "Subscription complete", Toast.LENGTH_SHORT).show();
                    }
                }));
    }
    private void scrollToBottom(final RecyclerView recyclerView) {
        // scroll to last item to get the view of last item
        final LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        final RecyclerView.Adapter adapter = recyclerView.getAdapter();
        final int lastItemPosition = adapter.getItemCount() - 1;

        layoutManager.scrollToPositionWithOffset(lastItemPosition, 0);
        recyclerView.post(new Runnable() {
            @Override
            public void run() {
                // then scroll to specific offset
                View target = layoutManager.findViewByPosition(lastItemPosition);
                if (target != null) {
                    int offset = recyclerView.getMeasuredHeight() - target.getMeasuredHeight();
                    layoutManager.scrollToPositionWithOffset(lastItemPosition, offset);
                }
            }
        });
    }

}
