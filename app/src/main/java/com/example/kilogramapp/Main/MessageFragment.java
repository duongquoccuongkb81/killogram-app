package com.example.kilogramapp.Main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.ApolloCallback;
import com.apollographql.apollo.ApolloSubscriptionCall;
import com.apollographql.apollo.api.OperationDataJsonSerializer;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;
import com.apollographql.apollo.fetcher.ApolloResponseFetchers;
import com.apollographql.apollo.rx2.Rx2Apollo;
import com.example.kilogramapp.Application.GlobalApplication;
import com.example.kilogramapp.CommentSubscription;
import com.example.kilogramapp.FindPostByIdQuery;
import com.example.kilogramapp.FollowPostSubscription;
import com.example.kilogramapp.GlobalConversationSubscription;
import com.example.kilogramapp.Login.LoginActivity;
import com.example.kilogramapp.MyConversationsQuery;
import com.example.kilogramapp.PostQuery;
import com.example.kilogramapp.R;
import com.example.kilogramapp.RecycleViewComponent.DataClass;
import com.example.kilogramapp.RecycleViewComponent.DataClassListChat;
import com.example.kilogramapp.RecycleViewComponent.DataCmt;
import com.example.kilogramapp.Utils.BinhLuanAdapter;
import com.example.kilogramapp.Utils.ListFriendAdapter;
import com.google.gson.Gson;
import com.orhanobut.logger.AndroidLogAdapter;
import com.orhanobut.logger.Logger;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subscribers.DisposableSubscriber;

public class MessageFragment extends Fragment {
    private static final String TAG = "MessageFragment";
    ArrayList<DataClassListChat> arrChat = new ArrayList<>();
    RecyclerView rwChat;
    ListFriendAdapter lst;
    EditText editText;
    private ApolloCall<MyConversationsQuery.Data> requestMyConversation;
    private Handler uiHandler = new Handler(Looper.getMainLooper());
    private final CompositeDisposable disposables = new CompositeDisposable();
    private GlobalApplication application;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_message, container, false);
        rwChat = (RecyclerView)view.findViewById(R.id.recycleViewChat);
        rwChat.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(rwChat.getContext(),LinearLayoutManager.VERTICAL,false);
        rwChat.setLayoutManager(layoutManager);
        application = (GlobalApplication) getActivity().getApplication();
        application.createSharedPreferences(getActivity());
        String token = application.getToken();
        if(token == null) {
            Toast.makeText(getActivity(), "Authenticated Fail", Toast.LENGTH_SHORT).show();
            application.removeStoredValue();
            Intent intentLogin = new Intent(getActivity(), LoginActivity.class);
            intentLogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intentLogin);
        }
        application.createApolloClient(token);
        populateData();
        subscribeConversations();
        return view;
    }

    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);
        if (menuVisible) {

        }
    }
    private ApolloCall.Callback<MyConversationsQuery.Data> requestMyConversationCallBack
            = new ApolloCallback<>(new ApolloCall.Callback<MyConversationsQuery.Data>() {
        @Override
        public void onResponse(@NotNull Response<MyConversationsQuery.Data> response) {
            if(response.hasErrors()) {
                String errorMessage = response.errors().get(0).message();
                Logger.e(errorMessage);
                if (errorMessage.contains("Authenticated Fail") ||  errorMessage.contains("jwt")) {
                    application.removeStoredValue();
                    Intent intentLogin = new Intent(getActivity(), LoginActivity.class);
                    intentLogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intentLogin);
                } else {
                    Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT).show();
                }
            } else {
                assert response.data() != null;
                String dataJSON = OperationDataJsonSerializer.serialize(response.data(), " ");
                Logger.d(dataJSON);
                response.data().myConversations().forEach(new Consumer<MyConversationsQuery.MyConversation>() {
                    @Override
                    public void accept(MyConversationsQuery.MyConversation myConversation) {
                        if (myConversation.visible() == true) {
                            DataClassListChat conversation = new DataClassListChat(myConversation);
                            arrChat.add(conversation);
                        }
                    }
                });

                initView();
            }
        }
        @Override
        public void onFailure(@NotNull ApolloException e) {
            Logger.e(e.getLocalizedMessage());
            Logger.e(String.valueOf(e.getCause()));
        }
    }, uiHandler);
    private void populateData() {
        requestMyConversation = application.apolloClient()
                .query(MyConversationsQuery
                        .builder()
                        .build()).responseFetcher(ApolloResponseFetchers.NETWORK_FIRST);
        requestMyConversation.enqueue(requestMyConversationCallBack);
    }
    public void initView(){

        //DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this,layoutManager.getOrientation());
        //recyclerView.addItemDecoration(dividerItemDecoration
        lst = new ListFriendAdapter(arrChat,getActivity().getApplicationContext());
        rwChat.setAdapter(lst);
    }
    private int findIndex (String conversationId) {
        for (int i = 0; i < arrChat.size(); i++) {
            if (arrChat.get(i).getConversation().id().equals(conversationId)) {
                return i;
            }
        }
        return -1;
    }
    private void subscribeConversations() {
        ApolloSubscriptionCall<GlobalConversationSubscription.Data> subscriptionCall = application.apolloClient()
                .subscribe(GlobalConversationSubscription.builder().build());
        disposables.add(Rx2Apollo.from(subscriptionCall)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSubscriber<Response<GlobalConversationSubscription.Data>>() {
                    @Override
                    public void onNext(Response<GlobalConversationSubscription.Data> dataResponse) {
                        Logger.d("Response Subscription Handler");
                        if (dataResponse.hasErrors()) {
                            String errorMessage = dataResponse.errors().get(0).message();
                            Logger.e(errorMessage);
                            if (errorMessage.contains("Authenticated Fail") || errorMessage.contains("jwt")) {
                                application.removeStoredValue();
                                Intent intentLogin = new Intent(getActivity(), LoginActivity.class);
                                intentLogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intentLogin);
                            }
                        } else {
                            String dataJSON = OperationDataJsonSerializer.serialize(dataResponse.data(), " ");
                            Logger.d(dataJSON);
                            GlobalConversationSubscription.Conversation data = dataResponse.data().conversation();
                            Gson gson = new Gson();
                            MyConversationsQuery.MyConversation conversation = gson.fromJson(gson.toJson(data.data()), MyConversationsQuery.MyConversation.class);
                            Logger.d(conversation);
                            //post.
                            if (data.mutation().rawValue().equals("CREATED")) {
                                if(data.data().visible() == true) {
                                    DataClassListChat newConversation = new DataClassListChat(conversation);
                                    int index = findIndex(conversation.id());
                                    if (index == -1) {
                                        arrChat.add(0, newConversation);
                                        lst.notifyDataSetChanged();
                                    } else {
                                        //arrChat.set(index, newConversation);
                                        arrChat.remove(index);
                                        lst.notifyDataSetChanged();
                                        arrChat.add(0, newConversation);
                                        lst.notifyItemChanged(0, newConversation);
                                    }
                                }
                            }
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        Logger.e(t.getMessage());
                        Logger.e(t.getCause().toString());
                        Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onComplete() {
                        Logger.d("Subscription Completed");
                        Toast.makeText(getActivity(), "Subscription complete", Toast.LENGTH_SHORT).show();
                    }
                }));
    }
}
