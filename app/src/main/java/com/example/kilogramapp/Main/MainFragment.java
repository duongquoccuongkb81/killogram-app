package com.example.kilogramapp.Main;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.ApolloCallback;
import com.apollographql.apollo.ApolloSubscriptionCall;
import com.apollographql.apollo.api.OperationDataJsonSerializer;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;
import com.apollographql.apollo.fetcher.ApolloResponseFetchers;
import com.apollographql.apollo.rx2.Rx2Apollo;
import com.example.kilogramapp.Application.GlobalApplication;
import com.example.kilogramapp.FollowPostSubscription;
import com.example.kilogramapp.Login.LoginActivity;
import com.example.kilogramapp.PostQuery;
import com.example.kilogramapp.R;
import com.example.kilogramapp.RecycleViewComponent.DataClass;
import com.example.kilogramapp.Utils.PostAdapter;
import com.orhanobut.logger.AndroidLogAdapter;
import com.orhanobut.logger.Logger;
import com.google.gson.Gson;

import org.jetbrains.annotations.NotNull;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.function.Consumer;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subscribers.DisposableSubscriber;

public class MainFragment extends Fragment {
    private static final String TAG = "MainFragment";
    boolean isLoading = false;
    boolean moreAndMore = true;
    private ProgressBar mProgressBar;
    ArrayList<DataClass> arrayList = new ArrayList<DataClass>();
    ArrayList<DataClass> newList = new ArrayList<DataClass>();
    RecyclerView recyclerView;
    TextView text;
    PostAdapter shopAdapter = new PostAdapter();
    private Handler uiHandler = new Handler(Looper.getMainLooper());
    private GlobalApplication application;
    private ApolloCall<PostQuery.Data> requestPosts;
    private ApolloCall<PostQuery.Data> requestLazyLoading;
    //Graphql Subscription using Rx2Java
    private final CompositeDisposable disposables = new CompositeDisposable();
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        Logger.d("Main Fragment setting up");
        recyclerView = (RecyclerView) view.findViewById(R.id.recycleView);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(recyclerView.getContext(),LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(layoutManager);
        application = (GlobalApplication) getActivity().getApplication();
        application.createSharedPreferences(getActivity());
        text = (TextView) view.findViewById(R.id.loaditem);
        mProgressBar = getActivity().findViewById(R.id.progress_horizontal);
        mProgressBar.getProgressDrawable().setColorFilter(
                Color.rgb(166, 220, 239), android.graphics.PorterDuff.Mode.SRC_IN);
        String token = application.getToken();
        if(token == null) {
            Toast.makeText(getActivity(), "Authenticated Fail", Toast.LENGTH_SHORT).show();
            application.removeStoredValue();
            Intent intentLogin = new Intent(getActivity(), LoginActivity.class);
            intentLogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intentLogin);
        }
        application.createApolloClient(token);
        populateData();
        listener();
        subscribePostAdded();
        initScrollListener();
        return view;
    }
    private ApolloCall.Callback<PostQuery.Data> requestPostsCallBack
            = new ApolloCallback<>(new ApolloCall.Callback<PostQuery.Data>() {
        @Override
        public void onResponse(@NotNull Response<PostQuery.Data> response) {
            mProgressBar.setProgress(100);
            mProgressBar.setVisibility(View.GONE);
            if(response.hasErrors()) {
                String errorMessage = response.errors().get(0).message();
                Logger.e(errorMessage);
                if (errorMessage.contains("Authenticated Fail") ||  errorMessage.contains("jwt")) {
                    application.removeStoredValue();
                    Intent intentLogin = new Intent(getActivity(), LoginActivity.class);
                    intentLogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intentLogin);
                }
            } else {
                String resultJSON = OperationDataJsonSerializer.serialize(response.data(), " ");
                Logger.d(resultJSON);
                response.data().followingPosts().forEach(new Consumer<PostQuery.FollowingPost>() {
                    @Override
                    public void accept(PostQuery.FollowingPost post) {
                        DataClass newPost = new DataClass(R.drawable.more,R.color.Basic,R.drawable.comment,"Thích",R.drawable.heart, post);
                        arrayList.add(newPost);
                    }
                });
                initView();
            }
        }
        @Override
        public void onFailure(@NotNull ApolloException e) {
            mProgressBar.setVisibility(View.GONE);
            Logger.e(e.getLocalizedMessage());
            Logger.e(String.valueOf(e.getCause()));
        }
    }, uiHandler);

    private ApolloCall.Callback<PostQuery.Data> requestLazyLoadingCallBack
            = new ApolloCallback<>(new ApolloCall.Callback<PostQuery.Data>() {
        @Override
        public void onResponse(@NotNull Response<PostQuery.Data> response) {
            //arrayList.remove(arrayList.size() - 1);
            mProgressBar.setProgress(100);
            mProgressBar.setVisibility(View.GONE);
            isLoading = false;
            if(response.hasErrors()) {
                String errorMessage = response.errors().get(0).message();
                Logger.e(errorMessage);
                if (errorMessage.contains("Authenticated Fail") ||  errorMessage.contains("jwt")) {
                    application.removeStoredValue();
                    Intent intentLogin = new Intent(getActivity(), LoginActivity.class);
                    intentLogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intentLogin);
                }
            } else {
                String resultJSON = OperationDataJsonSerializer.serialize(response.data(), " ");
                Logger.d(resultJSON);
                int currentSize = arrayList.size();
                if (response.data().followingPosts().size() > 0) {
                    response.data().followingPosts().forEach(new Consumer<PostQuery.FollowingPost>() {
                        @Override
                        public void accept(PostQuery.FollowingPost post) {
                            DataClass newPost = new DataClass(R.drawable.more,R.color.Basic,R.drawable.comment,"Thích",R.drawable.heart, post);
                            arrayList.add(newPost);
                        }
                    });
                    shopAdapter.notifyItemRangeInserted(currentSize, response.data().followingPosts().size());
                } else {
                    moreAndMore = false;
                }
            }
        }

        @Override
        public void onFailure(@NotNull ApolloException e) {
            //arrayList.remove(arrayList.size() - 1);
            mProgressBar.setVisibility(View.GONE);
            isLoading = false;
            Logger.e(e.getLocalizedMessage());
            Logger.e(String.valueOf(e.getCause()));
        }
    }, uiHandler);

    private void populateData() {
        int i = 0;
        String ID_person;
        int img_Avatar;
        String ID_post;
        String Chuoi;
        int React;
        String Like;
        int Binhluan;
        int mau;
        int nutchucnang;
        int countLike;
        int time;
        String contentWrite;


        requestPosts = application.apolloClient()
                        .query(PostQuery.builder().limit(5).build())
                        .responseFetcher(ApolloResponseFetchers.NETWORK_FIRST);
        mProgressBar.setProgress(40);
        mProgressBar.setVisibility(View.VISIBLE);
        requestPosts.enqueue(requestPostsCallBack);
    }
   public void initView(){
        //DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this,layoutManager.getOrientation());
        //recyclerView.addItemDecoration(dividerItemDecoration);
        shopAdapter = new PostAdapter(arrayList,getActivity().getApplicationContext());
        recyclerView.setAdapter(shopAdapter);
    }
    public void listener(){
        //text = (TextView) view.findViewById(R.id.loaditem);

        /*for(int i=0;i<arrayList.size();i++){
            shopAdapter.addItem(arrayList.get(i));
        }*/
        text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recyclerView.smoothScrollToPosition(0);
                newList.forEach(new Consumer<DataClass>() {
                    @Override
                    public void accept(DataClass dataClass) {
                        shopAdapter.addItem(dataClass);
                    }
                });
                newList.clear();
                text.setVisibility(View.GONE);
            }
        });
    }
    private void initScrollListener() {

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {

                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                if (!isLoading) {
                    if (linearLayoutManager != null && linearLayoutManager.findLastVisibleItemPosition() == arrayList.size() - 1 ) {
                        //bottom of list!
                        isLoading = true;
                        if(moreAndMore) {
                            loadMore();
                        }



                    }
                }
            }
        });
    }

    private void loadMore() {
        String lastPostId = arrayList.get(arrayList.size() - 1).mPost.id();
        Logger.d("Last postId : " + lastPostId);
        //arrayList.add(null);


        requestLazyLoading = application.apolloClient()
                .query(PostQuery.builder()
                        .limit(5)
                        .after(lastPostId).build())
                .responseFetcher(ApolloResponseFetchers.NETWORK_FIRST);
        mProgressBar.setProgress(40);
        mProgressBar.setVisibility(View.VISIBLE);
        requestLazyLoading.enqueue(requestLazyLoadingCallBack);


    }
    private void subscribePostAdded() {
        ApolloSubscriptionCall<FollowPostSubscription.Data> subscriptionCall = application.apolloClient()
                .subscribe(FollowPostSubscription.builder().build());
        disposables.add(Rx2Apollo.from(subscriptionCall)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSubscriber<Response<FollowPostSubscription.Data>>() {
                    @Override
                    public void onNext(Response<FollowPostSubscription.Data> dataResponse) {
                        Logger.d("Response Subscription Handler");
                        if (dataResponse.hasErrors()) {
                            String errorMessage = dataResponse.errors().get(0).message();
                            Logger.e(errorMessage);
                            if (errorMessage.contains("Authenticated Fail") ||  errorMessage.contains("jwt")) {
                                application.removeStoredValue();
                                Intent intentLogin = new Intent(getActivity(), LoginActivity.class);
                                intentLogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intentLogin);
                            }
                        } else {
                            String dataJSON = OperationDataJsonSerializer.serialize(dataResponse.data(), " ");
                            Logger.d(dataJSON);
                            FollowPostSubscription.Post data = dataResponse.data().post();
                            Gson gson = new Gson();
                            PostQuery.FollowingPost post = gson.fromJson(gson.toJson(data.data()), PostQuery.FollowingPost.class);
                            Logger.d(post);
                            //post.
                            if (data.mutation().rawValue().equals("CREATED")) {
//                                PostQuery.FollowingPost post = PostQuery.FollowingPost.class.cast(data.data());
//                                Logger.d(post);
                                text.setVisibility(View.VISIBLE);
                                DataClass newPost = new DataClass(R.drawable.more,R.color.Basic,R.drawable.comment,"Thích",R.drawable.heart, post);
                                newList.add(newPost);
                            } else if (data.mutation().rawValue().equals("DELETED"))
                            {
                                String postId = data.data().id();
                                Logger.d("postID removed " + postId);
                                int position = shopAdapter.GetPosition(postId);
                                Logger.d("position removed " + position);
                                shopAdapter.RemoveItemUI(position);
                            } else if (data.mutation().rawValue().equals("UPDATED")) {
                                DataClass newPost = new DataClass(R.drawable.more,R.color.Basic,R.drawable.comment,"Thích",R.drawable.heart, post);
                                Logger.d("postID updated " + post.id());
                                int position = shopAdapter.GetPosition(post.id());
                                Logger.d("position updated " + position);
                                if (position != -1) {
                                    shopAdapter.updateSingleItem(position, newPost);
                                }

                            }


                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        Logger.e(t.getMessage());
                        Logger.e(t.getCause().toString());
                        Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onComplete() {
                        Logger.d("Subscription Completed");
                        Toast.makeText(getActivity(), "Subscription complete", Toast.LENGTH_SHORT).show();
                    }
                })
        );

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        disposables.dispose();
        if(requestPosts != null)
            requestPosts.cancel();
        Logger.d("Main Fragment onDestroyView");
    }

//    @Override
//    public void onDestroy() {
//        super.onDestroy();
//        Logger.d("Main Fragment onDestroy");
//        if(requestPosts != null)
//            requestPosts.cancel();
//    }
}
