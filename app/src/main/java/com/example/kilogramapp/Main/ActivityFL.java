package com.example.kilogramapp.Main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.ApolloCallback;
import com.apollographql.apollo.api.OperationDataJsonSerializer;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;
import com.apollographql.apollo.fetcher.ApolloResponseFetchers;
import com.example.kilogramapp.Application.GlobalApplication;
import com.example.kilogramapp.CommentSubscription;
import com.example.kilogramapp.ConversationByIDQuery;
import com.example.kilogramapp.FavoritesByPostQuery;
import com.example.kilogramapp.Login.LoginActivity;
import com.example.kilogramapp.Profile.ProfileActivity;
import com.example.kilogramapp.R;
import com.example.kilogramapp.RecycleViewComponent.DataCmt;
import com.example.kilogramapp.RecycleViewComponent.DataLikeFollow;
import com.example.kilogramapp.RecycleViewComponent.NoiDungChat;
import com.example.kilogramapp.Utils.BinhLuanAdapter;
import com.example.kilogramapp.Utils.ChatAdapter;
import com.example.kilogramapp.Utils.LFAdapter;
import com.example.kilogramapp.Utils.PostAdapter;
import com.orhanobut.logger.Logger;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;


public class ActivityFL extends AppCompatActivity {
    ArrayList<DataLikeFollow> arrayLikedBys = new ArrayList<DataLikeFollow>();
    RecyclerView rwLF;
    LFAdapter lfAdapter;
    Context context = ActivityFL.this;
    private GlobalApplication application;
    private Handler uiHandler = new Handler(Looper.getMainLooper());
    private ApolloCall<FavoritesByPostQuery.Data> requestFavoritesByPost;
    private String postId;
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activty_likefollow);
        rwLF = (RecyclerView)findViewById(R.id.rwLF);
        rwLF.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        rwLF.setLayoutManager(layoutManager);
        ImageView img = (ImageView)findViewById(R.id.back_lf);
        TextView textView = (TextView)findViewById(R.id.usecase);
        textView.setText("Danh sách người thích");
        postId = getIntent().getStringExtra(PostAdapter.CHUYENTK);
        application = (GlobalApplication) getApplication();
        application.createSharedPreferences(context);
        String token = application.getToken();
        if(token == null) {
            Toast.makeText(context, "Authenticated Fail", Toast.LENGTH_SHORT).show();
            Intent intentLogin = new Intent(context, LoginActivity.class);
            intentLogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intentLogin);
        }
        application.createApolloClient(token);
        if (postId != null) {
            populateData();
        }
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }
    private ApolloCall.Callback<FavoritesByPostQuery.Data> FavoritesByPostCallBack
            = new ApolloCallback<>(new ApolloCall.Callback<FavoritesByPostQuery.Data>() {
        @Override
        public void onResponse(@NotNull Response<FavoritesByPostQuery.Data> response) {
            if(response.hasErrors()) {
                String errorMessage = response.errors().get(0).message();
                Logger.e(errorMessage);
                if (errorMessage.contains("Authenticated Fail") ||  errorMessage.contains("jwt")) {
                    application.removeStoredValue();
                    Intent intentLogin = new Intent(context, LoginActivity.class);
                    intentLogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intentLogin);
                } else {
                    Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show();
                }
            } else {
                assert response.data() != null;
                String dataJSON = OperationDataJsonSerializer.serialize(response.data(), " ");
                Logger.d(dataJSON);
                response.data().FavoritesByPost().forEach(favoritesByPost -> {
                    DataLikeFollow newData = new DataLikeFollow(favoritesByPost.likedBy());
                    arrayLikedBys.add(newData);
                });
                initView();
            }
        }
        @Override
        public void onFailure(@NotNull ApolloException e) {
            Logger.e(e.getLocalizedMessage());
            Logger.e(String.valueOf(e.getCause()));
        }
    }, uiHandler);


    private void populateData() {
        requestFavoritesByPost = application.apolloClient()
                .query(FavoritesByPostQuery
                        .builder()
                        .postId(postId)
                        .build()).responseFetcher(ApolloResponseFetchers.NETWORK_FIRST);
        requestFavoritesByPost.enqueue(FavoritesByPostCallBack);
    }

    public void initView(){
        lfAdapter= new LFAdapter(arrayLikedBys,this.context);
        rwLF.setAdapter(lfAdapter);
    }
}
