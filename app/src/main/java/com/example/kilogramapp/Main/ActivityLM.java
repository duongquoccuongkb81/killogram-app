package com.example.kilogramapp.Main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.ApolloCallback;
import com.apollographql.apollo.api.OperationDataJsonSerializer;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;
import com.apollographql.apollo.fetcher.ApolloResponseFetchers;
import com.example.kilogramapp.Application.GlobalApplication;
import com.example.kilogramapp.CommentSubscription;
import com.example.kilogramapp.FavoritesByPostQuery;
import com.example.kilogramapp.Login.LoginActivity;
import com.example.kilogramapp.PostQuery;
import com.example.kilogramapp.Profile.ProfileActivity;
import com.example.kilogramapp.Profile.ViewProfileActivity;
import com.example.kilogramapp.R;
import com.example.kilogramapp.RecycleViewComponent.DataCmt;
import com.example.kilogramapp.RecycleViewComponent.DataLikeFollow;
import com.example.kilogramapp.RecycleViewComponent.NoiDungChat;
import com.example.kilogramapp.UserFollowersQuery;
import com.example.kilogramapp.UserFollowingQuery;
import com.example.kilogramapp.Utils.BinhLuanAdapter;
import com.example.kilogramapp.Utils.ChatAdapter;
import com.example.kilogramapp.Utils.LFAdapter;
import com.example.kilogramapp.Utils.PostAdapter;
import com.google.gson.Gson;
import com.orhanobut.logger.Logger;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;


public class ActivityLM extends AppCompatActivity {
    ArrayList<DataLikeFollow> arrayData = new ArrayList<DataLikeFollow>();
    private static final String CHUYENID ="chuyenID";
    private static final String TYPE ="TYPE";
    RecyclerView rwLF;
    LFAdapter lfAdapter;
    Context context = ActivityLM.this;
    private GlobalApplication application;
    private Handler uiHandler = new Handler(Looper.getMainLooper());
    private ApolloCall<UserFollowersQuery.Data> requestUserFollowers;
    private ApolloCall<UserFollowingQuery.Data> requestUserFollowing;
    private String userQueryId;
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activty_likefollow);
        rwLF = (RecyclerView)findViewById(R.id.rwLF);
        rwLF.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        rwLF.setLayoutManager(layoutManager);
        ImageView img = (ImageView)findViewById(R.id.back_lf);
        TextView textView = (TextView)findViewById(R.id.usecase);

        application = (GlobalApplication) getApplication();
        application.createSharedPreferences(context);
        String token = application.getToken();
        if(token == null) {
            Toast.makeText(context, "Authenticated Fail", Toast.LENGTH_SHORT).show();
            Intent intentLogin = new Intent(context, LoginActivity.class);
            intentLogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intentLogin);
        }
        application.createApolloClient(token);
        userQueryId = getIntent().getStringExtra(CHUYENID);
        String type = getIntent().getStringExtra(TYPE);
        Logger.d(type);
        Logger.d(userQueryId);
        if (userQueryId != null) {
            if (type.equals("FOLLOWER")) {
                textView.setText("Danh sách người đã theo dõi user");
                populateFollowerData();
            } else if (type.equals("FOLLOWING")){
                textView.setText("Danh sách người user đang theo dõi");
                populateFollowingData();
            }
        }
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }
    private ApolloCall.Callback<UserFollowersQuery.Data> UserFollowersCallBack
            = new ApolloCallback<>(new ApolloCall.Callback<UserFollowersQuery.Data>() {
        @Override
        public void onResponse(@NotNull Response<UserFollowersQuery.Data> response) {
            if(response.hasErrors()) {
                String errorMessage = response.errors().get(0).message();
                Logger.e(errorMessage);
                if (errorMessage.contains("Authenticated Fail") ||  errorMessage.contains("jwt")) {
                    application.removeStoredValue();
                    Intent intentLogin = new Intent(context, LoginActivity.class);
                    intentLogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intentLogin);
                } else {
                    Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show();
                }
            } else {
                assert response.data() != null;
                String dataJSON = OperationDataJsonSerializer.serialize(response.data(), " ");
                Logger.d(dataJSON);
                Gson gson = new Gson();
                response.data().userFollowers().forEach(userFollower -> {
                    //PostQuery.FollowingPost post = gson.fromJson(gson.toJson(favorite.post()), PostQuery.FollowingPost.class);
                    FavoritesByPostQuery.LikedBy user = gson.fromJson(gson.toJson(userFollower), FavoritesByPostQuery.LikedBy.class);
                    DataLikeFollow newData = new DataLikeFollow(user);
                    arrayData.add(newData);
                });
                initView();
            }
        }
        @Override
        public void onFailure(@NotNull ApolloException e) {
            Toast.makeText(context, e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            Logger.e(e.getLocalizedMessage());
            Logger.e(String.valueOf(e.getCause()));
        }
    }, uiHandler);
    private ApolloCall.Callback<UserFollowingQuery.Data> UserFollowingCallBack
            = new ApolloCallback<>(new ApolloCall.Callback<UserFollowingQuery.Data>() {
        @Override
        public void onResponse(@NotNull Response<UserFollowingQuery.Data> response) {
            if(response.hasErrors()) {
                String errorMessage = response.errors().get(0).message();
                Logger.e(errorMessage);
                if (errorMessage.contains("Authenticated Fail") ||  errorMessage.contains("jwt")) {
                    application.removeStoredValue();
                    Intent intentLogin = new Intent(context, LoginActivity.class);
                    intentLogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intentLogin);
                } else {
                    Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show();
                }
            } else {
                assert response.data() != null;
                String dataJSON = OperationDataJsonSerializer.serialize(response.data(), " ");
                Logger.d(dataJSON);
                Gson gson = new Gson();
                response.data().userFollowing().forEach(userFollowing -> {
                    //PostQuery.FollowingPost post = gson.fromJson(gson.toJson(favorite.post()), PostQuery.FollowingPost.class);
                    FavoritesByPostQuery.LikedBy user = gson.fromJson(gson.toJson(userFollowing), FavoritesByPostQuery.LikedBy.class);
                    DataLikeFollow newData = new DataLikeFollow(user);
                    arrayData.add(newData);
                });
                initView();
            }
        }
        @Override
        public void onFailure(@NotNull ApolloException e) {
            Toast.makeText(context, e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            Logger.e(e.getLocalizedMessage());
            Logger.e(String.valueOf(e.getCause()));
        }
    }, uiHandler);
    private void populateFollowerData() {
        requestUserFollowers = application.apolloClient()
                .query(UserFollowersQuery
                        .builder()
                        .userId(userQueryId)
                        .build()).responseFetcher(ApolloResponseFetchers.NETWORK_FIRST);
        requestUserFollowers.enqueue(UserFollowersCallBack);
    }

    private void populateFollowingData() {
        requestUserFollowing = application.apolloClient()
                .query(UserFollowingQuery
                        .builder()
                        .userId(userQueryId)
                        .build()).responseFetcher(ApolloResponseFetchers.NETWORK_FIRST);
        requestUserFollowing.enqueue(UserFollowingCallBack);
    }

    public void initView(){
        lfAdapter= new LFAdapter(arrayData,context);
        rwLF.setAdapter(lfAdapter);
    }
}
