package com.example.kilogramapp.Main.Comment;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.ApolloCallback;
import com.apollographql.apollo.ApolloSubscriptionCall;
import com.apollographql.apollo.api.OperationDataJsonSerializer;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;
import com.apollographql.apollo.fetcher.ApolloResponseFetchers;
import com.apollographql.apollo.rx2.Rx2Apollo;
import com.example.kilogramapp.Application.GlobalApplication;
import com.example.kilogramapp.CommentSubscription;
import com.example.kilogramapp.CreateCommentMutation;
import com.example.kilogramapp.FavoritePostMutation;
import com.example.kilogramapp.FindPostByIdQuery;
import com.example.kilogramapp.FollowPostSubscription;
import com.example.kilogramapp.GetMyProfileQuery;
import com.example.kilogramapp.Login.LoginActivity;
import com.example.kilogramapp.PostQuery;
import com.example.kilogramapp.R;
import com.example.kilogramapp.RecycleViewComponent.DataClass;
import com.example.kilogramapp.RecycleViewComponent.DataCmt;
import com.example.kilogramapp.RemovePostMutation;
import com.example.kilogramapp.UnFavoritePostMutation;
import com.example.kilogramapp.UpdatePrivacyPostMutation;
import com.example.kilogramapp.UpdateTitlePostMutation;
import com.example.kilogramapp.Utils.BinhLuanAdapter;
import com.example.kilogramapp.Utils.GridImageAdapter;
import com.example.kilogramapp.Utils.PostAdapter;
import com.example.kilogramapp.type.CreateCommentInput;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.Gson;
import com.orhanobut.logger.Logger;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subscribers.DisposableSubscriber;


public class extend extends AppCompatActivity {
    private static final String TAG = "extend";
    private ArrayList<DataCmt> arrayComments = new ArrayList<>();
    private RecyclerView recyclerView1;
    private BinhLuanAdapter binhLuanAdapter;
    private Context context =extend.this;
    private NestedScrollView nestedScrollView;
    private EditText editText;
    private ImageView nutreact;
    private TextView postTitle;
    private TextView countlike;
    private TextView count_cmt;
    private ImageView send;
    private TextView time;
    private boolean favorited;
    private boolean published;
    private String originalTitle;
    //private String AuthId;
    private String postId;
    private String userId;
    private GlobalApplication application;
    private Handler uiHandler = new Handler(Looper.getMainLooper());
    private ApolloCall<FindPostByIdQuery.Data> requestFindPostById;
    private ApolloCall<FavoritePostMutation.Data> requestFavoritePost;
    private ApolloCall<UnFavoritePostMutation.Data> requestUnFavoritePost;
    private ApolloCall<CreateCommentMutation.Data> requestCreateComment;
    private ApolloCall<UpdatePrivacyPostMutation.Data> requestUpdatePrivacyPost;
    private ApolloCall<UpdateTitlePostMutation.Data> requestUpdateTitlePost;
    private ApolloCall<RemovePostMutation.Data> requestRemovePost;
    private FindPostByIdQuery.Post mPostData;
    private List<FindPostByIdQuery.Comment> listComment;
    private final CompositeDisposable disposables = new CompositeDisposable();

    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.extend);




        recyclerView1 = (RecyclerView)findViewById(R.id.rwComment2);
        recyclerView1.setHasFixedSize(true);
        nestedScrollView = (NestedScrollView) findViewById(R.id.manhinh);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        recyclerView1.setLayoutManager(layoutManager);
        application = (GlobalApplication) getApplication();
        application.createSharedPreferences(context);
        String token = application.getToken();
        if(token == null) {
            Toast.makeText(context, "Authenticated Fail", Toast.LENGTH_SHORT).show();
            Intent intentLogin = new Intent(context, LoginActivity.class);
            intentLogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intentLogin);
        }
        application.createApolloClient(token);
        userId = application.getUserId();
        if(PostAdapter.CHUYENID!=null)
        {
            postId = getIntent().getStringExtra(PostAdapter.CHUYENID);
        }
        else if(GridImageAdapter.CHUYENID!=null)
        {
            postId = getIntent().getStringExtra(GridImageAdapter.CHUYENID);
        }
        Logger.d(postId);
        if (postId != null) {
            populateData();
            subscribeComments();
        }
        ImageView back = (ImageView)findViewById(R.id.back_arrow);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }
    public void insertItem(DataCmt comment) {
        int position = arrayComments.size();
        Logger.d(position);
        arrayComments.add(position, comment);
        if (position == 0) {
            binhLuanAdapter.notifyDataSetChanged();
        } else {
            binhLuanAdapter.notifyItemInserted(position);
        }
    }
    private void removeItem(int index) {
        arrayComments.remove(index);
        binhLuanAdapter.notifyItemRemoved(index);
        //binhLuanAdapter.notifyDataSetChanged();
    }
    private ApolloCall.Callback<FindPostByIdQuery.Data> requestFindPostCallBack
            = new ApolloCallback<>(new ApolloCall.Callback<FindPostByIdQuery.Data>() {
        @Override
        public void onResponse(@NotNull Response<FindPostByIdQuery.Data> response) {
            if(response.hasErrors()) {
                String errorMessage = response.errors().get(0).message();
                Logger.e(errorMessage);
                if (errorMessage.contains("Authenticated Fail") ||  errorMessage.contains("jwt")) {
                    application.removeStoredValue();
                    Intent intentLogin = new Intent(context, LoginActivity.class);
                    intentLogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intentLogin);
                } else if (errorMessage.contains("Post not found")) {
                    Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show();
                    finish();
                }
            } else {
                assert response.data() != null;
                String dataJSON = OperationDataJsonSerializer.serialize(response.data(), " ");
                Logger.d(dataJSON);
                mPostData = response.data().post();
                listComment = mPostData.comments();
                preparePostUI();
                listComment.forEach(new Consumer<FindPostByIdQuery.Comment>() {
                    @Override
                    public void accept(FindPostByIdQuery.Comment comment) {
                        arrayComments.add(new DataCmt(comment));
                    }
                });
                initView();
            }
        }
        @Override
        public void onFailure(@NotNull ApolloException e) {
            Logger.e(e.getLocalizedMessage());
            Logger.e(String.valueOf(e.getCause()));
        }
    }, uiHandler);
    private ApolloCall.Callback<FavoritePostMutation.Data> requestFavoriteCallBack
            = new ApolloCallback<>(new ApolloCall.Callback<FavoritePostMutation.Data>() {
        @Override
        public void onResponse(@NotNull Response<FavoritePostMutation.Data> response) {
            nutreact.setEnabled(true);
            if(response.hasErrors()) {
                nutreact.setImageResource(R.drawable.heart);
                String errorMessage = response.errors().get(0).message();
                Logger.e(errorMessage);
                if (errorMessage.contains("Authenticated Fail") ||  errorMessage.contains("jwt")) {
                    application.removeStoredValue();
                    Intent intentLogin = new Intent(context, LoginActivity.class);
                    intentLogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intentLogin);
                } else if (errorMessage.contains("Post not found")) {
                    Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show();
                    finish();
                } else {
                    Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show();
                }
            } else {
                assert response.data() != null;
                String dataJSON = OperationDataJsonSerializer.serialize(response.data(), " ");
                Logger.d(dataJSON);
                FavoritePostMutation.FavoritePost favorite = response.data().favoritePost();
                countlike.setText(favorite.post().favoritesCount() + "");
                favorited = true;
            }
        }
        @Override
        public void onFailure(@NotNull ApolloException e) {
            nutreact.setEnabled(true);
            Toast.makeText(context, e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            Logger.e(e.getLocalizedMessage());
            Logger.e(String.valueOf(e.getCause()));
        }
    }, uiHandler);

    private ApolloCall.Callback<UnFavoritePostMutation.Data> requestUnFavoriteCallBack
            = new ApolloCallback<>(new ApolloCall.Callback<UnFavoritePostMutation.Data>() {
        @Override
        public void onResponse(@NotNull Response<UnFavoritePostMutation.Data> response) {
            nutreact.setEnabled(true);
            if(response.hasErrors()) {
                nutreact.setImageResource(R.drawable.heart_x);
                String errorMessage = response.errors().get(0).message();
                Logger.e(errorMessage);
                if (errorMessage.contains("Authenticated Fail") ||  errorMessage.contains("jwt")) {
                    application.removeStoredValue();
                    Intent intentLogin = new Intent(context, LoginActivity.class);
                    intentLogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intentLogin);
                } else if (errorMessage.contains("Unable to find post")) {
                    Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show();
                    finish();
                } else {
                    Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show();
                }
            } else {
                assert response.data() != null;
                String dataJSON = OperationDataJsonSerializer.serialize(response.data(), " ");
                Logger.d(dataJSON);
                UnFavoritePostMutation.UnFavoritePost unfavorite = response.data().unFavoritePost();
                countlike.setText(unfavorite.post().favoritesCount() + "");
                favorited = false;
            }
        }
        @Override
        public void onFailure(@NotNull ApolloException e) {
            nutreact.setEnabled(true);
            Toast.makeText(context, e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            Logger.e(e.getLocalizedMessage());
            Logger.e(String.valueOf(e.getCause()));
        }
    }, uiHandler);

    private ApolloCall.Callback<CreateCommentMutation.Data> requestCreateCommentCallBack
            = new ApolloCallback<>(new ApolloCall.Callback<CreateCommentMutation.Data>() {
        @Override
        public void onResponse(@NotNull Response<CreateCommentMutation.Data> response) {
            send.setEnabled(true);
            if(response.hasErrors()) {
                String errorMessage = response.errors().get(0).message();
                Logger.e(errorMessage);
                if (errorMessage.contains("Authenticated Fail") ||  errorMessage.contains("jwt")) {
                    application.removeStoredValue();
                    Intent intentLogin = new Intent(context, LoginActivity.class);
                    intentLogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intentLogin);
                } else if (errorMessage.contains("Unable to find post")) {
                    Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show();
                    finish();
                } else {
                    Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show();
                }
            } else {
                assert response.data() != null;
                String dataJSON = OperationDataJsonSerializer.serialize(response.data(), " ");
                Logger.d(dataJSON);
                CreateCommentMutation.CreateComment commentData = response.data().createComment();
                Gson gson = new Gson();
                FindPostByIdQuery.Comment comment = gson.fromJson(gson.toJson(commentData), FindPostByIdQuery.Comment.class);
                Logger.d(comment);
                DataCmt data = new DataCmt(comment);
                count_cmt.setText((Integer.parseInt(count_cmt.getText().toString()) + 1) + "");
                insertItem(data);
                nestedScrollView.post(new Runnable() {
                    @Override
                    public void run() {
                        nestedScrollView.fullScroll(View.FOCUS_DOWN);
                    }
                });
                recyclerView1.smoothScrollToPosition(recyclerView1.getAdapter().getItemCount() - 1);


            }
        }
        @Override
        public void onFailure(@NotNull ApolloException e) {
            send.setEnabled(true);
            Toast.makeText(context, e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            Logger.e(e.getLocalizedMessage());
            Logger.e(String.valueOf(e.getCause()));
        }
    }, uiHandler);

    private ApolloCall.Callback<RemovePostMutation.Data> requestRemovePostCallBack
            = new ApolloCallback<>(new ApolloCall.Callback<RemovePostMutation.Data>() {
        @Override
        public void onResponse(@NotNull Response<RemovePostMutation.Data> response) {
            if(response.hasErrors()) {
                String errorMessage = response.errors().get(0).message();
                Logger.e(errorMessage);
                if (errorMessage.contains("Authenticated Fail") ||  errorMessage.contains("jwt")) {
                    Intent intentLogin = new Intent(context, LoginActivity.class);
                    intentLogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    context.startActivity(intentLogin);

                } else {
                    Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show();
                }

            } else {
                String resultJSON = OperationDataJsonSerializer.serialize(response.data(), " ");
                Logger.d(resultJSON);
                finish();
            }
        }

        @Override
        public void onFailure(@NotNull ApolloException e) {
            Logger.e(e.getLocalizedMessage());
            Logger.e(e.getCause().toString());
            Toast.makeText(context, e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
        }
    }, uiHandler);

   private void populateData() {
       requestFindPostById = application.apolloClient()
               .query(FindPostByIdQuery
                       .builder()
                       .postId(postId)
                       .build()).responseFetcher(ApolloResponseFetchers.NETWORK_FIRST);
       requestFindPostById.enqueue(requestFindPostCallBack);

   }
   private void preparePostUI() {
       TextView username =(TextView)findViewById(R.id.textView3);
       ImageView avatar = (ImageView)findViewById(R.id.tamHinh3);
       ImageView postImage = (ImageView)findViewById(R.id.TamHinhND2);
       postTitle =(TextView)findViewById(R.id.Content2);
       countlike = (TextView)findViewById(R.id.countreact);
       count_cmt = (TextView)findViewById(R.id.countcmt);
       nutreact = (ImageView)findViewById(R.id.reactcmt);
       time = (TextView)findViewById(R.id.timebaiviet);
       CardView cardView = findViewById(R.id.card_view_post);

       //Xét privacy
       if (mPostData.published() == true) {
           published = true;
       } else {
           published = false;
       }

       originalTitle = mPostData.title();

       username.setText(mPostData.author().username());
       String url;
       if (mPostData.author().profile().avatar() == null) {
           url = "https://www.kindpng.com/picc/m/404-4042814_facebook-no-profile-png-download-default-headshot-transparent.png";
       } else {
           url = mPostData.author().profile().avatar().path();
       }
       Picasso.get().load(url).into(avatar);
       Picasso.get().load(mPostData.image().path()).into(postImage);
       postTitle.setText(mPostData.title());
       countlike.setText(mPostData.favoritesCount() + "");
       count_cmt.setText(mPostData.commentsCount() + "");
       time.setText(mPostData.createdAt()+"");


       send = (ImageView)findViewById(R.id.sendcmt);
       editText = (EditText)findViewById(R.id.edittext);
       ImageView nutchucnang = (ImageView)findViewById(R.id.chucnang3);
       if(mPostData.author().id().equals(userId))
       {

           nutchucnang.setOnClickListener(new View.OnClickListener() {
               boolean state;
               @Override
               public void onClick(View v) {
                   BottomSheetDialog bt = new BottomSheetDialog(context,R.style.bottomOption);
                   View bottomSheetView = LayoutInflater.from(context)
                           .inflate(
                                   R.layout.layout_bottom_chon,(LinearLayout)findViewById(R.id.bottomOption)
                           );
                   bt.setContentView(bottomSheetView);
                   bt.show();

                   EditText tx =(EditText) bt.findViewById(R.id.optiontext);
                   TextView pb =(TextView) bt.findViewById(R.id.Public);
                   TextView pr =(TextView)bt.findViewById(R.id.Private);
                   tx.setText(originalTitle);
                   ColorStateList oldColors =  pb.getTextColors();
                   if(published == true)
                   {
                       pb.setTextColor(Color.parseColor("#0576ff"));
                       state = true;

                   }
                   if(published == false)
                   {
                       pr.setTextColor(Color.parseColor("#0576ff"));
                       state = false;
                   }
                   bottomSheetView.findViewById(R.id.buttonPrivate).setOnClickListener(new View.OnClickListener() {
                       @Override
                       public void onClick(View v) {
                           state = false;
                           pr.setTextColor(Color.parseColor("#0576ff"));
                           pb.setTextColor(oldColors);
                       }
                   });
                   bottomSheetView.findViewById(R.id.buttonPublic).setOnClickListener(new View.OnClickListener() {
                       @Override
                       public void onClick(View v) {
                           state = true;
                           pb.setTextColor(Color.parseColor("#0576ff"));
                           pr.setTextColor(oldColors);
                       }
                   });
                   bottomSheetView.findViewById(R.id.suaquyen).setOnClickListener(new View.OnClickListener() {
                       /*Sửa quyền*/
                       @Override
                       public void onClick(View v) {
                           if (published == state) {
                               Toast.makeText(context, "Bài viết chưa cập nhật", Toast.LENGTH_LONG).show();
                           } else {
                               bt.dismiss();
                               requestUpdatePrivacyPost = application.apolloClient()
                                       .mutate(UpdatePrivacyPostMutation
                                               .builder()
                                               .postId(postId)
                                               .published(state)
                                               .build());
                               requestUpdatePrivacyPost.enqueue(new ApolloCall.Callback<UpdatePrivacyPostMutation.Data>() {
                                   @Override
                                   public void onResponse(@NotNull Response<UpdatePrivacyPostMutation.Data> response) {
                                       if (response.hasErrors()) {
                                           String errorMessage = response.errors().get(0).message();
                                           Logger.e(errorMessage);
                                           if (errorMessage.contains("Authenticated Fail") || errorMessage.contains("jwt")) {
                                               Intent intentLogin = new Intent(context, LoginActivity.class);
                                               intentLogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                               context.startActivity(intentLogin);
                                           } else {
                                               runOnUiThread(new Runnable() {
                                                   @Override
                                                   public void run() {
                                                       Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show();
                                                   }
                                               });
                                           }
                                       } else {
                                           String resultJSON = OperationDataJsonSerializer.serialize(response.data(), " ");
                                           Logger.d(resultJSON);
                                           runOnUiThread(new Runnable() {
                                               @Override
                                               public void run() {
                                                   published = state;
                                               }
                                           });
                                       }
                                   }

                                   @Override
                                   public void onFailure(@NotNull ApolloException e) {
                                       Logger.e(e.getLocalizedMessage());
                                       Logger.e(e.getCause().toString());
                                       runOnUiThread(new Runnable() {
                                           @Override
                                           public void run() {
                                               Toast.makeText(context, e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                                           }
                                       });
                                   }
                               });
                           }
                       }
                   });
                   bottomSheetView.findViewById(R.id.XoaBai).setOnClickListener(new View.OnClickListener() {
                      /*Xóa bài*/
                       @Override
                       public void onClick(View v) {
                           bt.dismiss();
                           RemovePost();
                       }
                   });
                   bottomSheetView.findViewById(R.id.suabai).setOnClickListener(new View.OnClickListener() {
                      /*Sửa bài*/
                       @Override
                       public void onClick(View v) {
                            String title = tx.getText().toString();
                            if (title.equals(originalTitle) || title.equals("")) {
                                Toast.makeText(context,"Bài viết chưa cập nhật",Toast.LENGTH_LONG).show();
                            } else {
                                requestUpdateTitlePost = application.apolloClient()
                                        .mutate(UpdateTitlePostMutation
                                                .builder().postId(postId).title(title)
                                                .build());
                                requestUpdateTitlePost.enqueue(new ApolloCall.Callback<UpdateTitlePostMutation.Data>() {
                                    @Override
                                    public void onResponse(@NotNull Response<UpdateTitlePostMutation.Data> response) {
                                        if(response.hasErrors()) {
                                            String errorMessage = response.errors().get(0).message();
                                            Logger.e(errorMessage);
                                            if (errorMessage.contains("Authenticated Fail") ||  errorMessage.contains("jwt")) {
                                                Intent intentLogin = new Intent(context, LoginActivity.class);
                                                intentLogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                context.startActivity(intentLogin);

                                            } else {
                                                Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show();
                                            }

                                        } else {
                                            String resultJSON = OperationDataJsonSerializer.serialize(response.data(), " ");
                                            Logger.d(resultJSON);
                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                   originalTitle = response.data().updatePost().title();
                                                   postTitle.setText(response.data().updatePost().title());
                                                }
                                            });
                                        }
                                    }

                                    @Override
                                    public void onFailure(@NotNull ApolloException e) {
                                        Logger.e(e.getLocalizedMessage());
                                        Logger.e(e.getCause().toString());
                                        Toast.makeText(context, e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                                    }
                                });
                                //dataClass.get(getAdapterPosition()).mPost.
                                bt.dismiss();
                            }
                       }
                   });
               }
           });
       } else {
           nutchucnang.setImageResource(0);
       }
       nutchucnang.setVisibility(View.VISIBLE);
       username.setVisibility(View.VISIBLE);
       avatar.setVisibility(View.VISIBLE);
       /*Nút chức năng*/
       if(mPostData.favorited()) {
           nutreact.setImageResource(R.drawable.heart_x);
           favorited = true;
       } else {
           nutreact.setImageResource(R.drawable.heart);
           favorited = false;
       }
       cardView.setVisibility(View.VISIBLE);



       nutreact.setOnClickListener(new View.OnClickListener(){
           @Override
           public void onClick(View v) {
               nutreact.setEnabled(false);
               if (favorited) {
                   nutreact.setImageResource(R.drawable.heart);
                   //Logger.d(countlike.getText());
                   countlike.setText((mPostData.favoritesCount() - 1) + "");
                   requestUnFavoritePost = application.apolloClient().mutate(UnFavoritePostMutation
                           .builder()
                           .postId(postId)
                           .build());
                   requestUnFavoritePost.enqueue(requestUnFavoriteCallBack);
               } else {
                   nutreact.setImageResource(R.drawable.heart_x);
                   countlike.setText((mPostData.favoritesCount() + 1) + "");
                   requestFavoritePost = application.apolloClient()
                           .mutate(FavoritePostMutation
                                   .builder()
                                   .postId(postId)
                                   .build());
                   requestFavoritePost.enqueue(requestFavoriteCallBack);
               }
           }
       });

       send.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               String content = editText.getText().toString();
               if(!isStringNull(content)) {
                   send.setEnabled(false);
                   editText.setText("");
                   //Turn off keyboard
                   View view = getCurrentFocus();
                   if (view != null) {
                       InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                       imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                   }
                   requestCreateComment = application.apolloClient()
                           .mutate(CreateCommentMutation
                                   .builder()
                                   .data(CreateCommentInput
                                           .builder()
                                           .postId(postId)
                                           .text(content)
                                           .build())
                                   .build());
                   requestCreateComment.enqueue(requestCreateCommentCallBack);
//
//                   //arrayList1.add(0,new DataCmt("a","trinhmapdit",chuoibinhluan,"2 phút trước"));
//                   insertItem(position);
//                   editText.setText("");
//                   //initView();
//                   recyclerView1.smoothScrollToPosition(recyclerView1.getAdapter().getItemCount());
               }
           }
       });

   }
    private boolean isStringNull(String string){
        Log.d(TAG, "isStringNull: checking string if null");

        if (string.equals("")){
            return true;
        }
        return false;
    }
    public void initView(){

        //DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this,layoutManager.getOrientation());
        //recyclerView.addItemDecoration(dividerItemDecoration);
        binhLuanAdapter= new BinhLuanAdapter(arrayComments,context);
        recyclerView1.setAdapter(binhLuanAdapter);
        //recyclerView1.scrollToPosition(recyclerView1.getAdapter().getItemCount()-1);
    }
    private int  findIndexComment(String commentId) {
       for (int i = 0; i< arrayComments.size(); i++) {
           if (arrayComments.get(i).getmComment().id().equals(commentId)) {
               return i;
           }
       }
       return -1;
    }
    private void subscribeComments() {
        ApolloSubscriptionCall<CommentSubscription.Data> subscriptionCall = application.apolloClient()
                .subscribe(CommentSubscription.builder().postId(postId).build());
        disposables.add(Rx2Apollo.from(subscriptionCall)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSubscriber<Response<CommentSubscription.Data>>() {
                    @Override
                    public void onNext(Response<CommentSubscription.Data> dataResponse) {
                        Logger.d("Response Subscription Handler");
                        if (dataResponse.hasErrors()) {
                            String errorMessage = dataResponse.errors().get(0).message();
                            Logger.e(errorMessage);
                            if (errorMessage.contains("Authenticated Fail") ||  errorMessage.contains("jwt")) {
                                application.removeStoredValue();
                                Intent intentLogin = new Intent(context, LoginActivity.class);
                                intentLogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intentLogin);
                            } else {
                                Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            String dataJSON = OperationDataJsonSerializer.serialize(dataResponse.data(), " ");
                            Logger.d(dataJSON);
                            CommentSubscription.Comment data = dataResponse.data().comment();
                            //post.
                            if (data.mutation().rawValue().equals("CREATED")) {
                                if (!data.data().author().id().equals(userId)) {
                                    Gson gson = new Gson();
                                    FindPostByIdQuery.Comment comment = gson.fromJson(gson.toJson(data.data()), FindPostByIdQuery.Comment.class);
                                    Logger.d(comment);
                                    DataCmt newComment = new DataCmt(comment);
                                    insertItem(newComment);
                                    count_cmt.setText((Integer.parseInt(count_cmt.getText().toString()) + 1) + "");
                                }
                            } else if (data.mutation().rawValue().equals("DELETED"))
                            {
                                int index = findIndexComment(data.data().id());
                                if (index != -1) {
                                    removeItem(index);
                                }
//                                String postId = data.data().id();
//                                Logger.d("postID removed " + postId);
//                                int position = shopAdapter.GetPosition(postId);
//                                Logger.d("position removed " + position);
//                                shopAdapter.RemoveItemUI(position);
                            }

                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        Logger.e(t.getMessage());
                        Logger.e(t.getCause().toString());
                        Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onComplete() {
                        Logger.d("Subscription Completed");
                        Toast.makeText(context, "Subscription complete", Toast.LENGTH_SHORT).show();
                    }
                }
                )
        );
    }

    public void RemovePost()
    {

            requestRemovePost = application.apolloClient()
                    .mutate(RemovePostMutation
                            .builder().postId(postId).build());
            requestRemovePost.enqueue(requestRemovePostCallBack);

    }
}