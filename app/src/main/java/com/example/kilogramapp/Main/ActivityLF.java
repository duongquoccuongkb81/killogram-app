package com.example.kilogramapp.Main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.kilogramapp.CommentSubscription;
import com.example.kilogramapp.Profile.ProfileActivity;
import com.example.kilogramapp.R;
import com.example.kilogramapp.RecycleViewComponent.DataCmt;
import com.example.kilogramapp.RecycleViewComponent.DataLikeFollow;
import com.example.kilogramapp.RecycleViewComponent.NoiDungChat;
import com.example.kilogramapp.Utils.BinhLuanAdapter;
import com.example.kilogramapp.Utils.ChatAdapter;
import com.example.kilogramapp.Utils.LFAdapter;
import com.example.kilogramapp.Utils.PostAdapter;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class ActivityLF extends AppCompatActivity {
    ArrayList<DataLikeFollow> nd = new ArrayList<DataLikeFollow>();
    RecyclerView rwLF;
    LFAdapter lfAdapter;
    Context context;
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activty_likefollow);
        rwLF = (RecyclerView)findViewById(R.id.rwLF);
        rwLF.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        rwLF.setLayoutManager(layoutManager);
        ImageView img = (ImageView)findViewById(R.id.back_lf);
        TextView textView = (TextView)findViewById(R.id.usecase);
        textView.setText("Danh sách người thích");
        String followid="hihi";
//        if(ProfileActivity.CHUYENID!=null)
//        {
//            followid = getIntent().getStringExtra(ProfileActivity.CHUYENID);
//            textView.setText(followid);
//
//        }
//        else if(ProfileActivity.CHUYENFF!=null)
//        {
//            followid = getIntent().getStringExtra(ProfileActivity.CHUYENFF);
//            textView.setText(followid);
//        }
//        else
//        {
//            if(PostAdapter.CHUYENTK!=null)
//            {
//                followid = getIntent().getStringExtra(PostAdapter.CHUYENTK);
//                textView.setText(followid);
//            }
//        }
        populateData();
        initView();
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               finish();
            }
        });


    }
    private void populateData() {

    }

    public void initView(){

        //DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this,layoutManager.getOrientation());
        //recyclerView.addItemDecoration(dividerItemDecoration);
        lfAdapter= new LFAdapter(nd,this.context);
        rwLF.setAdapter(lfAdapter);
        //recyclerView1.scrollToPosition(recyclerView1.getAdapter().getItemCount()-1);
    }
}
