package com.example.kilogramapp.Share;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.ApolloCallback;
import com.apollographql.apollo.api.FileUpload;
import com.apollographql.apollo.api.OperationDataJsonSerializer;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;
import com.example.kilogramapp.Application.GlobalApplication;
import com.example.kilogramapp.CreatePostMutation;
import com.example.kilogramapp.FavoritePostMutation;
import com.example.kilogramapp.FindPostByIdQuery;
import com.example.kilogramapp.Login.LoginActivity;
import com.example.kilogramapp.Main.MainActivity;
import com.example.kilogramapp.R;
import com.example.kilogramapp.Utils.BottomNavigationViewHelper;
import com.example.kilogramapp.Utils.FileUtils;
import com.example.kilogramapp.type.CreatePostInput;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;
import com.orhanobut.logger.Logger;
import com.squareup.picasso.Picasso;
import com.tbruyelle.rxpermissions2.RxPermissions;

import org.jetbrains.annotations.NotNull;

import java.io.File;

@SuppressLint("Registered")
public class ShareActivity extends AppCompatActivity {
    private static final String TAG = "ShareActivity";
    private static final int ACTIVITY_NUM = 2;
    private static final int PICK_IMAGE_REQUEST = 10;
    private boolean published = true;
    private ImageView imageChoose;
    private EditText inputTitle;
    private LinearLayout btnPublish;
    private LinearLayout btnPrivate;
    private TextView pr;
    private TextView pb;
    private TextView btnUploadPost;
    private ImageView backArrow;
    private Uri mImageUri;
    private ProgressBar mProgressBar;
    private GlobalApplication application;
    private Handler uiHandler = new Handler(Looper.getMainLooper());
    private ApolloCall<CreatePostMutation.Data> requestCreatePost;

    private Context mContext = ShareActivity.this;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post);
        backArrow = (ImageView)findViewById(R.id.trove);
        Log.d(TAG, "onCreate: started");
        imageChoose = findViewById(R.id.ImageChoose);
        inputTitle = findViewById(R.id.input_title);
        btnPublish = findViewById(R.id.buttonPublic);
        btnPrivate = findViewById(R.id.buttonPrivate);
        btnUploadPost = findViewById(R.id.upload_post);
        mProgressBar = findViewById(R.id.progress_horizontal);
        pr = findViewById(R.id.Private);
        pb = findViewById(R.id.Public);
        initUI();
        final RxPermissions rxPermissions = new RxPermissions(this);
        rxPermissions.request(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .subscribe(granted -> {
                    if (granted) {

                    } else {

                    }
                });
        application = (GlobalApplication) getApplication();
        application.createSharedPreferences(mContext);
        String token = application.getToken();
        if(token == null) {
            Toast.makeText(mContext, "Authenticated Fail", Toast.LENGTH_SHORT).show();
            Intent intentLogin = new Intent(mContext, LoginActivity.class);
            intentLogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intentLogin);
        }
        application.createApolloClient(token);
        //setupBottomNavigationView();
    }

    private ApolloCall.Callback<CreatePostMutation.Data> requestCreatePostCallBack
            = new ApolloCallback<>(new ApolloCall.Callback<CreatePostMutation.Data>() {
        @Override
        public void onResponse(@NotNull Response<CreatePostMutation.Data> response) {
            btnUploadPost.setEnabled(true);

            if(response.hasErrors()) {
                mProgressBar.setVisibility(View.GONE);
                String errorMessage = response.errors().get(0).message();
                Logger.e(errorMessage);
                if (errorMessage.contains("Authenticated Fail") ||  errorMessage.contains("jwt")) {
                    application.removeStoredValue();
                    Intent intentLogin = new Intent(mContext, LoginActivity.class);
                    intentLogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intentLogin);
                } else {
                    Toast.makeText(mContext, errorMessage, Toast.LENGTH_SHORT).show();
                }
            } else {
                mProgressBar.setProgress(100);
                assert response.data() != null;
                String dataJSON = OperationDataJsonSerializer.serialize(response.data(), " ");
                Logger.d(dataJSON);
                Intent intent = new Intent(mContext, MainActivity.class);
                startActivity(intent);
            }
        }
        @Override
        public void onFailure(@NotNull ApolloException e) {
            btnUploadPost.setEnabled(true);
            mProgressBar.setVisibility(View.GONE);
            Toast.makeText(mContext, e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            Logger.e(e.getLocalizedMessage());
            Logger.e(String.valueOf(e.getCause()));
        }
    }, uiHandler);

    private void initUI() {
        backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        imageChoose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFileChooser();
            }
        });
        ColorStateList oldColors =  pb.getTextColors();
        btnPublish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                published = true;
                pb.setTextColor(Color.parseColor("#0576ff"));
                pr.setTextColor(oldColors);
            }
        });
        btnPrivate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                published = false;
                pr.setTextColor(Color.parseColor("#0576ff"));
                pb.setTextColor(oldColors);
            }
        });
        btnUploadPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isStringNull(inputTitle.getText().toString())) {
                    Toast.makeText(mContext, "Vui lòng nhập tiêu đề của bài viết", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (mImageUri == null) {
                    Toast.makeText(mContext, "Vui lòng chọn ảnh của bài viết", Toast.LENGTH_SHORT).show();
                    return;
                }
                String title = inputTitle.getText().toString();
                String mimetype = FileUtils.getMimeType(mContext, mImageUri);
                Logger.d(mimetype);
                String fullPath = FileUtils.getPath(mContext, mImageUri);
                Logger.d(fullPath);
                if(fullPath == null) {
                    Toast.makeText(mContext, "Vui lòng chọn lại ảnh của bài viết", Toast.LENGTH_SHORT).show();
                    return;
                }
                assert fullPath != null;
                File mfile = new File(fullPath);
                CreatePostInput inputPost = CreatePostInput.builder()
                        .title(title)
                        .published(published)
                        .image(new FileUpload(mimetype, mfile))
                        .build();
                btnUploadPost.setEnabled(false);
                mProgressBar.setProgress(40);
                mProgressBar.setVisibility(View.VISIBLE);
                requestCreatePost = application.apolloClient()
                        .mutate(CreatePostMutation
                                .builder()
                                .data(inputPost)
                                .build());
                requestCreatePost.enqueue(requestCreatePostCallBack);
            }
        });
    }

    private void openFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        //intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.setAction(Intent.ACTION_PICK);
        startActivityForResult(intent, PICK_IMAGE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK
                    && data != null && data.getData() != null) {
                mImageUri = data.getData();
                Long fileSize = FileUtils.getSize(mContext, mImageUri);
                Logger.d(fileSize);
                if (fileSize > 10000000) {
                    Toast.makeText(mContext, "Can't upload file bigger than 10MB", Toast.LENGTH_SHORT).show();
                    return;
                }
                int height = imageChoose.getHeight();
                LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) imageChoose.getLayoutParams();
                layoutParams.width = 1000;
                layoutParams.height = height;
                imageChoose.setLayoutParams(layoutParams);
                Picasso.get().load(mImageUri).into(imageChoose);
                imageChoose.setImageURI(mImageUri);
        }
    }
    private boolean isStringNull(String string){
        Log.d(TAG, "isStringNull: checking string if null");

        if (string.equals("")){
            return true;
        }
        return false;
    }
}
