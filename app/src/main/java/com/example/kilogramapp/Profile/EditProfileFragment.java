package com.example.kilogramapp.Profile;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.ApolloCallback;
import com.apollographql.apollo.api.FileUpload;
import com.apollographql.apollo.api.OperationDataJsonSerializer;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;
import com.apollographql.apollo.fetcher.ApolloResponseFetchers;
import com.example.kilogramapp.Application.GlobalApplication;
import com.example.kilogramapp.CreatePostMutation;
import com.example.kilogramapp.Login.LoginActivity;
import com.example.kilogramapp.Main.MainActivity;
import com.example.kilogramapp.R;
import com.example.kilogramapp.UploadAvatarMutation;
import com.example.kilogramapp.UserProfileByIDQuery;
import com.example.kilogramapp.Utils.FileUtils;
import com.example.kilogramapp.Utils.GlideImageLoader;
import com.example.kilogramapp.type.GenderType;
import com.orhanobut.logger.Logger;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.Objects;

import static android.app.Activity.RESULT_OK;

public class EditProfileFragment extends Fragment {
    private static final String TAG = "EditProfileFragment";
    private static final int PICK_IMAGE_REQUEST = 10;
    private ImageView mProfilePhoto;
    private TextView mBtnChangeAvatar;
    private EditText mUserName;
    private EditText mName;
    private EditText mBio;
    private EditText mEmail;
    private RadioGroup mRadioGroup;
    private GlobalApplication application;
    private String userId;
    private Uri mImageUri;
    private Handler uiHandler = new Handler(Looper.getMainLooper());
    private ApolloCall<UserProfileByIDQuery.Data> requestProfile;
    private ApolloCall<UploadAvatarMutation.Data> requestUploadAvatar;
    private UserProfileByIDQuery.User mUserData;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile_setting_editprofile, container, false);
        mProfilePhoto = (ImageView) view.findViewById(R.id.profile_photo);
        mBtnChangeAvatar = view.findViewById(R.id.changeProfilePhoto);
        mUserName = view.findViewById(R.id.editUserName);
        mName = view.findViewById(R.id.editName);
        mBio = view.findViewById(R.id.editBio);
        mEmail = view.findViewById(R.id.editEmail);
        mRadioGroup = view.findViewById(R.id.genderRadioGroup);
        application = (GlobalApplication) getActivity().getApplication();
        application.createSharedPreferences(getActivity());
        String token = application.getToken();
        if(token == null) {
            Toast.makeText(getActivity(), "Authenticated Fail", Toast.LENGTH_SHORT).show();
            Intent intentLogin = new Intent(getActivity(), LoginActivity.class);
            intentLogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intentLogin);
        }
        application.createApolloClient(token);
        userId = application.getUserId();
        ImageView mBackArrow = (ImageView) view.findViewById(R.id.back_arrow);
        mBackArrow.setOnClickListener(v -> {
            Objects.requireNonNull(getActivity()).finish();
        });
        if (userId != null) {
            populateData();
        }
        return view;
    }

    private ApolloCall.Callback<UserProfileByIDQuery.Data> UserProfileCallBack
            = new ApolloCallback<>(new ApolloCall.Callback<UserProfileByIDQuery.Data>() {
        @Override
        public void onResponse(@NotNull Response<UserProfileByIDQuery.Data> response) {

            if(response.hasErrors()) {
                String errorMessage = response.errors().get(0).message();
                Logger.e(errorMessage);
                if (errorMessage.contains("Authenticated Fail") ||  errorMessage.contains("jwt")) {
                    application.removeStoredValue();
                    Intent intentLogin = new Intent(getActivity(), LoginActivity.class);
                    intentLogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intentLogin);
                } else {
                    Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT).show();
                }
            } else {
                assert response.data() != null;
                String dataJSON = OperationDataJsonSerializer.serialize(response.data(), " ");
                Logger.d(dataJSON);
                mUserData = response.data().user();
                setUpUI();
            }
        }
        @Override
        public void onFailure(@NotNull ApolloException e) {
            Toast.makeText(getActivity(), e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            Logger.e(e.getLocalizedMessage());
            Logger.e(String.valueOf(e.getCause()));
        }
    }, uiHandler);
    private ApolloCall.Callback<UploadAvatarMutation.Data> UploadAvatarCallBack
            = new ApolloCallback<>(new ApolloCall.Callback<UploadAvatarMutation.Data>() {
        @Override
        public void onResponse(@NotNull Response<UploadAvatarMutation.Data> response) {

            if(response.hasErrors()) {
                String errorMessage = response.errors().get(0).message();
                Logger.e(errorMessage);
                if (errorMessage.contains("Authenticated Fail") ||  errorMessage.contains("jwt")) {
                    application.removeStoredValue();
                    Intent intentLogin = new Intent(getActivity(), LoginActivity.class);
                    intentLogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intentLogin);
                } else {
                    Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT).show();
                }
            } else {
                assert response.data() != null;
                String dataJSON = OperationDataJsonSerializer.serialize(response.data(), " ");
                Logger.d(dataJSON);
                Picasso.get().load(mImageUri).into(mProfilePhoto);
            }
        }
        @Override
        public void onFailure(@NotNull ApolloException e) {
            Toast.makeText(getActivity(), e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            Logger.e(e.getLocalizedMessage());
            Logger.e(String.valueOf(e.getCause()));
        }
    }, uiHandler);
    private void populateData() {
        requestProfile = application.apolloClient()
                .query(UserProfileByIDQuery
                        .builder()
                        .id(userId)
                        .build()).responseFetcher(ApolloResponseFetchers.NETWORK_FIRST);
        requestProfile.enqueue(UserProfileCallBack);
    }
    private void setUpUI() {
        mUserName.setText(mUserData.username());
        mName.setText(mUserData.profile().name());
        mBio.setText(mUserData.profile().bio() == null ? "" : mUserData.profile().bio());
        mEmail.setText(mUserData.email());
        if (mUserData.gender() == GenderType.MALE) {
            mRadioGroup.check(R.id.radio_Male);
        } else if (mUserData.gender() == GenderType.FEMALE) {
            mRadioGroup.check(R.id.radio_Female);
        }
        String url = getActivity().getResources().getString(R.string.default_avatar);
        if (mUserData.profile().avatar() != null) {
            url = mUserData.profile().avatar().path();
        }
        Picasso.get().load(url).into(mProfilePhoto);
        mBtnChangeAvatar.setOnClickListener(v -> {
            Logger.d("ABC");
            openFileChooser();
        });
    }
    private void openFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        //intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.setAction(Intent.ACTION_PICK);
        startActivityForResult(intent, PICK_IMAGE_REQUEST);
        //getActivity().startActivityForResult();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK
                && data != null && data.getData() != null) {
            mImageUri = data.getData();
            Long fileSize = FileUtils.getSize(getActivity(), mImageUri);
            Logger.d(fileSize);
            if (fileSize > 10000000) {
                Toast.makeText(getActivity(), "Can't upload file bigger than 10MB", Toast.LENGTH_SHORT).show();
                return;
            }
            String mimetype = FileUtils.getMimeType(getActivity(), mImageUri);
            Logger.d(mimetype);
            String fullPath = FileUtils.getPath(getActivity(), mImageUri);
            Logger.d(fullPath);
            assert fullPath != null;
            File mfile = new File(fullPath);
            requestUploadAvatar = application.apolloClient()
                    .mutate(UploadAvatarMutation
                            .builder()
                            .image(new FileUpload(mimetype, mfile))
                            .build());
            requestUploadAvatar.enqueue(UploadAvatarCallBack);
        }

    }
}
