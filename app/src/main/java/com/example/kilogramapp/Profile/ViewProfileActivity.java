package com.example.kilogramapp.Profile;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.ApolloCallback;
import com.apollographql.apollo.api.OperationDataJsonSerializer;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;
import com.apollographql.apollo.fetcher.ApolloResponseFetchers;
import com.example.kilogramapp.Application.GlobalApplication;
import com.example.kilogramapp.FindUserByIDQuery;
import com.example.kilogramapp.FollowMutation;
import com.example.kilogramapp.GetMyProfileQuery;
import com.example.kilogramapp.JoinTheConversationMutation;
import com.example.kilogramapp.Login.LoginActivity;
import com.example.kilogramapp.Main.ActivityChat;
import com.example.kilogramapp.Main.ActivityLF;
import com.example.kilogramapp.Main.ActivityLM;
import com.example.kilogramapp.R;
import com.example.kilogramapp.UnFollowMutation;
import com.example.kilogramapp.Utils.BottomNavigationViewHelper;
import com.example.kilogramapp.Utils.GlideImageLoader;
import com.example.kilogramapp.Utils.GridImageAdapter;
import com.example.kilogramapp.Utils.GridImageComponent.GridViewItem;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;
import com.orhanobut.logger.Logger;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Consumer;

public class ViewProfileActivity extends AppCompatActivity {
    private static final String TAG = "ViewProfileActivity";
    private TextView toolbarText;
    private ImageView backArrow;
    private TextView BtnFollow;
    private static final int NUM_GRID_COLUMNS = 3;
    private Context mContext = ViewProfileActivity.this;
    private ProgressBar mProgressBar;
    private ImageView mProfileImage;
    private GlobalApplication application;
    private boolean followed;
    private TextView BtnSendMessage;
    private String userQueryId;
    public static final String CHUYENID ="chuyenID";
    public static final String CHUYENFF ="chuyenID";
    Handler uiHandler = new Handler(Looper.getMainLooper());
    ApolloCall<FindUserByIDQuery.Data> requestFindUserById;
    ApolloCall<FollowMutation.Data> requestFollowUser;
    ApolloCall<UnFollowMutation.Data> requestUnFollowUser;
    ApolloCall<JoinTheConversationMutation.Data> requestJoinConversation;

    List<FindUserByIDQuery.Post> myPosts;
    FindUserByIDQuery.User myUserData;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_profile);
        toolbarText = findViewById(R.id.profileSetting);
        backArrow = findViewById(R.id.back_arrow);
        toolbarText.setVisibility(View.GONE);
        Log.d(TAG, "onCreate: started");
        backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        application = (GlobalApplication) getApplication();
        application.createSharedPreferences(mContext);
        mProgressBar = (ProgressBar) findViewById(R.id.profileProgressBar);
        mProfileImage = (ImageView) findViewById(R.id.profile_image);
        String token = application.getToken();
        if(token == null) {
            Toast.makeText(mContext, "Authenticated Fail", Toast.LENGTH_SHORT).show();
            Intent intentLogin = new Intent(mContext, LoginActivity.class);
            intentLogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intentLogin);
        }
        application.createApolloClient(token);
        requestUserProfileData();
        setupBottomNavigationView();

    }
    private ApolloCall.Callback<FindUserByIDQuery.Data> FindUserByIdCallback
            = new ApolloCallback<>(new ApolloCall.Callback<FindUserByIDQuery.Data>() {

        @Override
        public void onResponse(@NotNull Response<FindUserByIDQuery.Data> response) {
            mProgressBar.setVisibility(View.GONE);
            if (response.hasErrors()) {
                String errorMessage = response.errors().get(0).message();
                Logger.e(errorMessage);
                if (errorMessage.contains("Authenticated Fail") ||  errorMessage.contains("jwt")) {
                    application.removeStoredValue();
                    Intent intentLogin = new Intent(mContext, LoginActivity.class);
                    intentLogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intentLogin);
                } else if (errorMessage.contains("User not found")){
                    Toast.makeText(mContext, "User not found!", Toast.LENGTH_SHORT).show();
                    finish();
                }
            } else {
                assert response.data() != null;
                String dataJSON = OperationDataJsonSerializer.serialize(response.data(), " ");
                Logger.d(dataJSON);
                myUserData = response.data().user();
                myPosts = response.data().user().posts();
                setupCountInfo();
                setupCenterProfileInfo();
                setProfileImage();
                tempGridSetup();
            }
        }

        @Override
        public void onFailure(@NotNull ApolloException e) {
            Logger.e(Objects.requireNonNull(e.getLocalizedMessage()));
        }
    }, uiHandler);
    private ApolloCall.Callback<FollowMutation.Data> FollowUserCallBack
            = new ApolloCallback<>(new ApolloCall.Callback<FollowMutation.Data>() {

        @Override
        public void onResponse(@NotNull Response<FollowMutation.Data> response) {
            mProgressBar.setVisibility(View.GONE);
            BtnFollow.setEnabled(true);
            if (response.hasErrors()) {
                String errorMessage = response.errors().get(0).message();
                Logger.e(errorMessage);
                if (errorMessage.contains("Authenticated Fail") ||  errorMessage.contains("jwt")) {
                    application.removeStoredValue();
                    Intent intentLogin = new Intent(mContext, LoginActivity.class);
                    intentLogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intentLogin);
                } else if (errorMessage.contains("Follow")) {
                    Toast.makeText(mContext, errorMessage, Toast.LENGTH_SHORT).show();
                }
            } else {
                assert response.data() != null;
                String dataJSON = OperationDataJsonSerializer.serialize(response.data(), " ");
                Logger.d(dataJSON);
                BtnFollow.setText("Đang theo dõi");
                BtnFollow.setBackgroundResource(R.drawable.grey_border_rounded);
                followed = true;
            }
        }

        @Override
        public void onFailure(@NotNull ApolloException e) {
            BtnFollow.setEnabled(true);
            Logger.e(Objects.requireNonNull(e.getLocalizedMessage()));
        }
    }, uiHandler);
    private ApolloCall.Callback<UnFollowMutation.Data> UnFollowUserCallBack
            = new ApolloCallback<>(new ApolloCall.Callback<UnFollowMutation.Data>() {

        @Override
        public void onResponse(@NotNull Response<UnFollowMutation.Data> response) {
            mProgressBar.setVisibility(View.GONE);
            BtnFollow.setEnabled(true);
            if (response.hasErrors()) {
                String errorMessage = response.errors().get(0).message();
                Logger.e(errorMessage);
                if (errorMessage.contains("Authenticated Fail") ||  errorMessage.contains("jwt")) {
                    application.removeStoredValue();
                    Intent intentLogin = new Intent(mContext, LoginActivity.class);
                    intentLogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intentLogin);
                } else if (errorMessage.contains("Follow")) {
                    Toast.makeText(mContext, errorMessage, Toast.LENGTH_SHORT).show();
                }
            } else {
                assert response.data() != null;
                String dataJSON = OperationDataJsonSerializer.serialize(response.data(), " ");
                Logger.d(dataJSON);
                BtnFollow.setText("Theo dõi");
                BtnFollow.setBackgroundResource(R.drawable.blue_background_rounded);
                followed = false;
            }
        }

        @Override
        public void onFailure(@NotNull ApolloException e) {
            BtnFollow.setEnabled(true);
            Logger.e(Objects.requireNonNull(e.getLocalizedMessage()));
        }
    }, uiHandler);

    private ApolloCall.Callback<JoinTheConversationMutation.Data> JoinConversationCallBack
            = new ApolloCallback<>(new ApolloCall.Callback<JoinTheConversationMutation.Data>() {

        @Override
        public void onResponse(@NotNull Response<JoinTheConversationMutation.Data> response) {
            mProgressBar.setVisibility(View.GONE);
            BtnSendMessage.setEnabled(true);
            if (response.hasErrors()) {
                String errorMessage = response.errors().get(0).message();
                Logger.e(errorMessage);
                if (errorMessage.contains("Authenticated Fail") ||  errorMessage.contains("jwt")) {
                    application.removeStoredValue();
                    Intent intentLogin = new Intent(mContext, LoginActivity.class);
                    intentLogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intentLogin);
                } else {
                    Toast.makeText(mContext, errorMessage, Toast.LENGTH_SHORT).show();
                }
            } else {
                assert response.data() != null;
                String dataJSON = OperationDataJsonSerializer.serialize(response.data(), " ");
                Logger.d(dataJSON);
                Intent intent = new Intent(mContext, ActivityChat.class);
                intent.putExtra(CHUYENID, response.data().joinTheConversation().id());
                startActivity(intent);
            }
        }

        @Override
        public void onFailure(@NotNull ApolloException e) {
            BtnSendMessage.setEnabled(true);
            Logger.e(Objects.requireNonNull(e.getLocalizedMessage()));
        }
    }, uiHandler);
    private void requestUserProfileData() {
        userQueryId = getIntent().getStringExtra("userId");
        requestFindUserById = application.apolloClient()
                .query(FindUserByIDQuery
                        .builder()
                        .id(userQueryId)
                        .build()).responseFetcher(ApolloResponseFetchers.NETWORK_FIRST);
        requestFindUserById.enqueue(FindUserByIdCallback);
    }
    private void setupCountInfo() {
        TextView tvPostCount = findViewById(R.id.tvPosts);
        TextView tvFollowerCount = findViewById(R.id.tvFollowers);
        TextView tvFollowingCount = findViewById(R.id.tvFollowing);
        TextView ffwer = findViewById(R.id.textFollowers);
        TextView fflowing = findViewById(R.id.textFollowing);
        ffwer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent a = new Intent(v.getContext(), ActivityLM.class);
                a.putExtra(CHUYENID,userQueryId);
                a.putExtra("TYPE", "FOLLOWER");
                v.getContext().startActivity(a);
            }
        });
        fflowing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent a = new Intent(v.getContext(), ActivityLM.class);
                a.putExtra(CHUYENID,userQueryId);
                a.putExtra("TYPE", "FOLLOWING");
                v.getContext().startActivity(a);
            }
        });
        //Logger.d(tvPostCount);
        tvPostCount.setText(String.valueOf(myPosts.size()));
        tvFollowerCount.setText(String.valueOf(myUserData.followers().size()));
        tvFollowingCount.setText(String.valueOf(myUserData.following().size()));
        tvPostCount.setVisibility(View.VISIBLE);
        tvFollowerCount.setVisibility(View.VISIBLE);
        tvFollowingCount.setVisibility(View.VISIBLE);
    }
    private void setupCenterProfileInfo() {
        TextView tvProfileName = findViewById(R.id.display_name);
        TextView tvProfileBio = findViewById(R.id.display_description_profile);
        BtnFollow = findViewById(R.id.textBtnFollow);
        BtnSendMessage = findViewById(R.id.textBtnSendMessage);
        if (myUserData.followed().rawValue().equals("True")) {
            BtnFollow.setText("Đang theo dõi");
            BtnFollow.setBackgroundResource(R.drawable.grey_border_rounded);
            followed = true;
        } else if (myUserData.followed().rawValue().equals("False")) {
            followed = false;
        }
        BtnFollow.setVisibility(View.VISIBLE);
        BtnSendMessage.setVisibility(View.VISIBLE);
        tvProfileName.setText(myUserData.profile().name());
        tvProfileName.setVisibility(View.VISIBLE);
        if(myUserData.profile().bio() != null){
            tvProfileBio.setText(myUserData.profile().bio());
            tvProfileBio.setVisibility(View.VISIBLE);
        }
        BtnFollow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BtnFollow.setEnabled(false);
                requestFollowUser = application.apolloClient()
                        .mutate(FollowMutation
                                .builder()
                                .userId(userQueryId)
                                .build());
                requestUnFollowUser = application.apolloClient()
                        .mutate(UnFollowMutation
                                .builder()
                                .userId(userQueryId)
                                .build());
                if (followed) {
                    requestUnFollowUser.enqueue(UnFollowUserCallBack);
                } else {
                    requestFollowUser.enqueue(FollowUserCallBack);
                }
            }
        });
        BtnSendMessage.setOnClickListener(v -> {
            requestJoinConversation = application.apolloClient()
                    .mutate(JoinTheConversationMutation
                            .builder()
                            .userId(userQueryId)
                            .build());
            BtnSendMessage.setEnabled(false);
            requestJoinConversation.enqueue(JoinConversationCallBack);
        });
    }
    private void setProfileImage() {
        Log.d(TAG, "setProfileImage: setting up profile image");
        String imgURL;
        if (myUserData.profile().avatar() == null) {
            imgURL = "https://www.kindpng.com/picc/m/404-4042814_facebook-no-profile-png-download-default-headshot-transparent.png";
        } else {
            imgURL = myUserData.profile().avatar().path();
        }
        //imgURL = "https://www.kindpng.com/picc/m/404-4042814_facebook-no-profile-png-download-default-headshot-transparent.png";
        GlideImageLoader.setImage(mContext, imgURL, mProfileImage, "");
    }

    private void tempGridSetup(){
        ArrayList<GridViewItem> posts = new ArrayList<>();
        myPosts.forEach(new Consumer<FindUserByIDQuery.Post>() {
            @Override
            public void accept(FindUserByIDQuery.Post post) {
                posts.add(new GridViewItem(post.id(), post.image().path()));
            }
        });
        setupImageGrid(posts);
    }

    private void setupImageGrid(ArrayList<GridViewItem> posts) {
        GridView gridView = (GridView) findViewById(R.id.centerProfileGridView);
        int gridWidth = getResources().getDisplayMetrics().widthPixels;
        int imageWidth = gridWidth/NUM_GRID_COLUMNS;
        gridView.setColumnWidth(imageWidth);

        GridImageAdapter adapter = new GridImageAdapter(mContext, R.layout.layout_grid_imageview, "", posts);
        gridView.setAdapter(adapter);
    }
    private void setupBottomNavigationView() {
        Log.d(TAG, "setupBottomNavigationView: setting up");
        BottomNavigationViewEx bnve = (BottomNavigationViewEx) findViewById(R.id.bottomNavViewBar);
        BottomNavigationViewHelper.setupBottomNavigationView(bnve);
        BottomNavigationViewHelper.enableNavigation(mContext, bnve);
        Menu menu = bnve.getMenu();
        MenuItem menuItem = menu.getItem(1);
        menuItem.setChecked(true);
    }
}
