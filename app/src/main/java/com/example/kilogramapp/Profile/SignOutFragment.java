package com.example.kilogramapp.Profile;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.ApolloCallback;
import com.apollographql.apollo.api.OperationDataJsonSerializer;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;
import com.example.kilogramapp.Application.GlobalApplication;
import com.example.kilogramapp.Login.LoginActivity;
import com.example.kilogramapp.LogoutMutation;
import com.example.kilogramapp.MyConversationsQuery;
import com.example.kilogramapp.R;
import com.example.kilogramapp.RecycleViewComponent.DataClassListChat;
import com.orhanobut.logger.Logger;

import org.jetbrains.annotations.NotNull;

import java.util.function.Consumer;

import io.reactivex.disposables.CompositeDisposable;

public class SignOutFragment extends Fragment {
    private static final String TAG = "SignOutFragment";

    private ProgressBar mProgressBar;
    private TextView tvSignout;
    private Handler uiHandler = new Handler(Looper.getMainLooper());
    private ApolloCall<LogoutMutation.Data> requestLogout;
    private GlobalApplication application;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile_setting_signout, container, false);
        tvSignout = (TextView) view.findViewById(R.id.comfirmSignout);
        mProgressBar= (ProgressBar) view.findViewById(R.id.progressbar);
        Button btnComfirmSignout = (Button) view.findViewById(R.id.yes);
        Button btnNo = (Button) view.findViewById(R.id.no);
        mProgressBar.setVisibility(View.GONE);
        application = (GlobalApplication) getActivity().getApplication();
        application.createSharedPreferences(getActivity());
        String token = application.getToken();
        if(token == null) {
            Toast.makeText(getActivity(), "Authenticated Fail", Toast.LENGTH_SHORT).show();
            application.removeStoredValue();
            Intent intentLogin = new Intent(getActivity(), LoginActivity.class);
            intentLogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intentLogin);
        }
        application.createApolloClient(token);

        btnComfirmSignout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               requestLogout = application.apolloClient()
                       .mutate(LogoutMutation
                               .builder()
                               .build());
               mProgressBar.setVisibility(View.VISIBLE);
               requestLogout.enqueue(requestLogoutCallBack);

            }
        });
        btnNo.setOnClickListener(v -> {
            getActivity().finish();
        });
        return view;
    }
    private ApolloCall.Callback<LogoutMutation.Data> requestLogoutCallBack
            = new ApolloCallback<>(new ApolloCall.Callback<LogoutMutation.Data>() {
        @Override
        public void onResponse(@NotNull Response<LogoutMutation.Data> response) {
            mProgressBar.setVisibility(View.GONE);
            if(response.hasErrors()) {
                String errorMessage = response.errors().get(0).message();
                Logger.e(errorMessage);
                if (errorMessage.contains("Authenticated Fail") ||  errorMessage.contains("jwt")) {
                    application.removeStoredValue();
                    Intent intentLogin = new Intent(getActivity(), LoginActivity.class);
                    intentLogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intentLogin);
                } else {
                    Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT).show();
                }
            } else {
                assert response.data() != null;
                String dataJSON = OperationDataJsonSerializer.serialize(response.data(), " ");
                Logger.d(dataJSON);
                application.removeStoredValue();
                Intent intentLogin = new Intent(getActivity(), LoginActivity.class);
                intentLogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intentLogin);
            }
        }
        @Override
        public void onFailure(@NotNull ApolloException e) {
            mProgressBar.setVisibility(View.GONE);
            Toast.makeText(getActivity(), e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            Logger.e(e.getLocalizedMessage());
            Logger.e(String.valueOf(e.getCause()));
        }
    }, uiHandler);
}
