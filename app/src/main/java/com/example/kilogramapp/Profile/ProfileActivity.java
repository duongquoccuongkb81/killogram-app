package com.example.kilogramapp.Profile;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.ApolloCallback;
import com.apollographql.apollo.api.OperationDataJsonSerializer;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;
import com.apollographql.apollo.fetcher.ApolloResponseFetchers;
import com.example.kilogramapp.Application.GlobalApplication;
import com.example.kilogramapp.GetMyProfileQuery;
import com.example.kilogramapp.Login.LoginActivity;
import com.example.kilogramapp.Main.ActivityLF;
import com.example.kilogramapp.Main.ActivityLM;
import com.example.kilogramapp.R;
import com.example.kilogramapp.Utils.BottomNavigationViewHelper;
import com.example.kilogramapp.Utils.GlideImageLoader;
import com.example.kilogramapp.Utils.GridImageAdapter;
import com.example.kilogramapp.Utils.GridImageComponent.GridViewItem;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;
import com.orhanobut.logger.Logger;

import org.jetbrains.annotations.NotNull;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Consumer;

@SuppressLint("Registered")
public class ProfileActivity extends AppCompatActivity {
    private static final String TAG = "ProfileActivity";
    private static final int ACTIVITY_NUM = 4;
    private static final int NUM_GRID_COLUMNS = 3;
    private Context mContext = ProfileActivity.this;
    private ProgressBar mProgressBar;
    private ImageView mProfileImage;
    private GlobalApplication application;
    Handler uiHandler = new Handler(Looper.getMainLooper());
    ApolloCall<GetMyProfileQuery.Data> myProfileActivityCall;
    List<GetMyProfileQuery.Post> myPosts;
    GetMyProfileQuery.Me myProfileData;
    public static final String CHUYENID ="chuyenID";
    public static final String TYPE ="TYPE";
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        Log.d(TAG, "onCreate: started");
        application = (GlobalApplication) getApplication();
        application.createSharedPreferences(mContext);
        mProgressBar = (ProgressBar) findViewById(R.id.profileProgressBar);
        mProfileImage = (ImageView) findViewById(R.id.profile_image);
        requestProfileData();

        setupBottomNavigationView();
    }
    private ApolloCall.Callback<GetMyProfileQuery.Data> myProfileDataCallback
            = new ApolloCallback<>(new ApolloCall.Callback<GetMyProfileQuery.Data>() {

        @Override
        public void onResponse(@NotNull Response<GetMyProfileQuery.Data> response) {
            mProgressBar.setVisibility(View.GONE);
            if (response.hasErrors()) {
                String errorMessage = response.errors().get(0).message();
                Logger.e(errorMessage);
                if (errorMessage.contains("Authenticated Fail") ||  errorMessage.contains("jwt")) {
                    application.removeStoredValue();
                    Intent intentLogin = new Intent(mContext, LoginActivity.class);
                    intentLogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intentLogin);
                }
            } else {
                assert response.data() != null;
                String dataJSON = OperationDataJsonSerializer.serialize(response.data(), " ");
                Logger.d(dataJSON);
                myProfileData = response.data().me();
                myPosts = response.data().me().posts();
                setupToolbar();
                setupCountInfo();
                setupCenterProfileInfo();
                setProfileImage();
                tempGridSetup();
            }
        }

        @Override
        public void onFailure(@NotNull ApolloException e) {
            Logger.e(Objects.requireNonNull(e.getLocalizedMessage()));
        }
    }, uiHandler);
    private void requestProfileData() {
        String token = application.getToken();
        if(token == null) {
            Toast.makeText(mContext, "Authenticated Fail", Toast.LENGTH_SHORT).show();
            Intent intentLogin = new Intent(mContext, LoginActivity.class);
            intentLogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intentLogin);
        }
        application.createApolloClient(token);
        myProfileActivityCall = application.apolloClient()
                .query(GetMyProfileQuery.builder().build())
                .responseFetcher(ApolloResponseFetchers.NETWORK_FIRST);
        myProfileActivityCall.enqueue(myProfileDataCallback);
    }

    private void tempGridSetup(){
        ArrayList<GridViewItem> posts = new ArrayList<>();
        myPosts.forEach(new Consumer<GetMyProfileQuery.Post>() {
            @Override
            public void accept(GetMyProfileQuery.Post post) {
                posts.add(new GridViewItem(post.id(), post.image().path()));
            }
        });
        setupImageGrid(posts);
    }

    private void setupImageGrid(ArrayList<GridViewItem> posts) {
        GridView gridView = (GridView) findViewById(R.id.centerProfileGridView);
        int gridWidth = getResources().getDisplayMetrics().widthPixels;
        int imageWidth = gridWidth/NUM_GRID_COLUMNS;
        gridView.setColumnWidth(imageWidth);

        GridImageAdapter adapter = new GridImageAdapter(mContext, R.layout.layout_grid_imageview, "", posts);
        gridView.setAdapter(adapter);
    }
    private void setProfileImage() {
        Log.d(TAG, "setProfileImage: setting up profile image");
        String imgURL;
        if(myProfileData.profile().avatar() == null) {
            imgURL = "https://www.kindpng.com/picc/m/404-4042814_facebook-no-profile-png-download-default-headshot-transparent.png";
        } else {
            imgURL = myProfileData.profile().avatar().path();
        }
        //imgURL = "https://www.kindpng.com/picc/m/404-4042814_facebook-no-profile-png-download-default-headshot-transparent.png";
        GlideImageLoader.setImage(mContext, imgURL, mProfileImage, "");
    }

    private void setupBottomNavigationView() {
        Log.d(TAG, "setupBottomNavigationView: setting up");
        BottomNavigationViewEx bnve = (BottomNavigationViewEx) findViewById(R.id.bottomNavViewBar);
        BottomNavigationViewHelper.setupBottomNavigationView(bnve);
        BottomNavigationViewHelper.enableNavigation(mContext, bnve);
        Menu menu = bnve.getMenu();
        MenuItem menuItem = menu.getItem(ACTIVITY_NUM);
        menuItem.setChecked(true);
    }

    private void setupToolbar() {
        // Tạo sự kiện click vào item trên top toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.profileToolBar);
        setSupportActionBar(toolbar);
        TextView profileUserName = findViewById(R.id.profileUserName);
        profileUserName.setText(myProfileData.username());
        profileUserName.setVisibility(View.VISIBLE);
        ImageView profileMenu = (ImageView) findViewById(R.id.images_profileMenu);
        TextView ffer = (TextView)findViewById(R.id.textFollowers);
        TextView ffing = (TextView)findViewById(R.id.textFollowing);

        ffer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent a = new Intent(v.getContext(), ActivityLM.class);
                a.putExtra(CHUYENID,application.getUserId());
                a.putExtra(TYPE, "FOLLOWER");
                v.getContext().startActivity(a);
            }
        });
        ffing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent a = new Intent(v.getContext(), ActivityLM.class);
                a.putExtra(CHUYENID,application.getUserId());
                a.putExtra(TYPE, "FOLLOWING");
                v.getContext().startActivity(a);
            }
        });
        profileMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: navigating to account settings. ");
                Intent intent = new Intent(mContext, AccountSettingsActivity.class);
                startActivity(intent);
            }
        });

        TextView textView =(TextView)findViewById(R.id.textEditProfile);
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: navigating to account settings. ");
                Intent intent = new Intent(mContext, AccountSettingsActivity.class);
                startActivity(intent);
            }
        });
    }

    private void setupCountInfo() {
        TextView tvPostCount = findViewById(R.id.tvPosts);
        TextView tvFollowerCount = findViewById(R.id.tvFollowers);
        TextView tvFollowingCount = findViewById(R.id.tvFollowing);
        //Logger.d(tvPostCount);
        tvPostCount.setText(String.valueOf(myPosts.size()));
        tvFollowerCount.setText(String.valueOf(myProfileData.followers().size()));
        tvFollowingCount.setText(String.valueOf(myProfileData.following().size()));
        tvPostCount.setVisibility(View.VISIBLE);
        tvFollowerCount.setVisibility(View.VISIBLE);
        tvFollowingCount.setVisibility(View.VISIBLE);
    }

    private void setupCenterProfileInfo() {
        TextView tvProfileName = findViewById(R.id.display_name);
        TextView tvProfileBio = findViewById(R.id.display_description_profile);
        tvProfileName.setText(myProfileData.profile().name());
        tvProfileName.setVisibility(View.VISIBLE);
        if(myProfileData.profile().bio() != null){
            tvProfileBio.setText(myProfileData.profile().bio());
            tvProfileBio.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(myProfileActivityCall != null)
            myProfileActivityCall.cancel();
    }
}
