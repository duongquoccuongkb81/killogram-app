package com.example.kilogramapp.Application;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.apollographql.apollo.ApolloClient;
import com.apollographql.apollo.api.Operation;
import com.apollographql.apollo.api.ResponseField;
import com.apollographql.apollo.cache.normalized.CacheKey;
import com.apollographql.apollo.cache.normalized.CacheKeyResolver;
import com.apollographql.apollo.cache.normalized.NormalizedCacheFactory;
import com.apollographql.apollo.cache.normalized.lru.EvictionPolicy;
import com.apollographql.apollo.cache.normalized.lru.LruNormalizedCacheFactory;
import com.apollographql.apollo.cache.normalized.sql.ApolloSqlHelper;
import com.apollographql.apollo.cache.normalized.sql.SqlNormalizedCacheFactory;
import com.apollographql.apollo.subscription.SubscriptionConnectionParams;
import com.apollographql.apollo.subscription.WebSocketSubscriptionTransport;
import com.example.kilogramapp.R;

import org.jetbrains.annotations.NotNull;

import java.util.Map;

import okhttp3.OkHttpClient;

public class GlobalApplication extends Application {
    private static final String TAG = "SetupApolloAndroid";
    private static final String URL = "https://cuong-kilogram-app.herokuapp.com/graphql";
    private static final String WS_URL = "ws://cuong-kilogram-app.herokuapp.com/graphql";
    private static final String SQL_CACHE_NAME = "testgraphql";
    private ApolloClient apolloClient;
    private NormalizedCacheFactory normalizedCacheFactory;
    private CacheKeyResolver cacheKeyResolver;
    private SharedPreferences sharedPref;
    private SharedPreferences.Editor editor;

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate: setting up");

        ApolloSqlHelper apolloSqlHelper = new ApolloSqlHelper(this, SQL_CACHE_NAME);
        normalizedCacheFactory = new LruNormalizedCacheFactory(EvictionPolicy.NO_EVICTION)
                .chain(new SqlNormalizedCacheFactory(apolloSqlHelper));

        cacheKeyResolver =  new CacheKeyResolver() {
            @NotNull
            @Override
            public CacheKey fromFieldRecordSet(@NotNull ResponseField field, @NotNull Map<String, Object> recordSet) {
                return formatCacheKey((String) recordSet.get("id"));
            }

            @NotNull @Override
            public CacheKey fromFieldArguments(@NotNull ResponseField field, @NotNull Operation.Variables variables) {
                return formatCacheKey((String) field.resolveArgument("id", variables));
            }

            private CacheKey formatCacheKey(String id) {
                if (id == null || id.isEmpty()) {
                    return CacheKey.NO_KEY;
                } else {
                    return CacheKey.from(id);
                }
            }
        };

    }



    private void getApolloClientWithToken(String token) {
        Log.d(TAG, "getApolloClientWithToken: ");
        OkHttpClient okHttpClient = new OkHttpClient()
                .newBuilder()
                .addInterceptor(chain ->
                        chain.proceed(chain.request()
                                .newBuilder()
                                .addHeader("Authorization", "Bearer " + token)
                                .build()))
                .build();
        SubscriptionConnectionParams connectionParams = new SubscriptionConnectionParams();
        connectionParams.put("Authorization", "Bearer " + token);
        WebSocketSubscriptionTransport.Factory factory = new WebSocketSubscriptionTransport.Factory(WS_URL, okHttpClient);
        apolloClient = ApolloClient.builder()
                .serverUrl(URL)
                .okHttpClient(okHttpClient)
                .normalizedCache(normalizedCacheFactory, cacheKeyResolver)
                .subscriptionConnectionParams(connectionParams)
                .subscriptionTransportFactory(factory)
                .build();

    }

    private void getApolloClientWithoutToken() {
        Log.d(TAG, "getApolloClientWithoutToken: ");
        apolloClient = ApolloClient.builder()
                .serverUrl(URL)
                .normalizedCache(normalizedCacheFactory, cacheKeyResolver)
                .build();

    }


    public ApolloClient apolloClient() {
        return apolloClient;
    }

    public void createApolloClientWithoutToken() {
        getApolloClientWithoutToken();
    }
    public void createApolloClient(String token) {
        getApolloClientWithToken(token);
    }
    public void createSharedPreferences (@NotNull Context context) {
        sharedPref = context.getSharedPreferences(
                getString(R.string.stored_private_key), context.MODE_PRIVATE);
        editor = sharedPref.edit();
    }
    public SharedPreferences stored() { return sharedPref; }
    public void storeToken(String token) {
        editor.putString(getString(R.string.stored_token), token);
        editor.apply();
    }
    public String getToken() {
        String token = sharedPref.getString(getString(R.string.stored_token), null);
        return token;
    }
    public void storeUserId(String userId) {
        editor.putString(getString(R.string.stored_userid), userId);
        editor.apply();
    }
    public String getUserId() {
        String userId = sharedPref.getString(getString(R.string.stored_userid), null);
        return userId;
    }
    public void removeStoredValue() {
        editor.remove(getString(R.string.stored_token));
        editor.remove(getString(R.string.stored_userid));
        editor.apply();
    }
}
