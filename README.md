# kilogram-app

## 0. Check out Branch Dev

## 1. Requirement

Android 8.1+ (API 27+)

Edit URL, WS_URL in GlobalApplication

Run with Local Server

```bash
private static final String URL = "http://10.0.2.2/graphql";
private static final String WS_URL = "ws://10.0.2.2/graphql";
```

Run with Heroku Server

```bash
private static final String URL = "https://cuong-kilogram-app.herokuapp.com/graphql";
private static final String WS_URL = "ws://cuong-kilogram-app.herokuapp.com/graphql";
```

## 1. License

[MIT](https://choosealicense.com/licenses/mit/)
